<?php

include 'includes/sqlConnect.php';
include 'includes/fonction.php';
include 'includes/scraping.php';
include 'includes/scraping_seloger.php';
include 'includes/scraping_paruvendu.php';
include 'includes/scraping_foncia.php';
include 'includes/scraping_safti.php';
include 'includes/arrondissement.php';

$donnees = array();
if(isset($_GET['code_postal']) && !empty($_GET['code_postal'])){
	$code_postal_annonces_ventes = $_GET['code_postal'];
}else{
	exit(json_encode($donnees));
}

$sqlAnnoncesVentes = "SELECT 
						* 
						FROM annonces_ventes 
						WHERE code_postal_annonces_ventes = :code_postal_annonces_ventes 
						AND DATE_ADD(DATE(date_scrap_annonces_ventes), INTERVAL 5 DAY) > DATE(NOW())
					";
$stmt_annonces_ventes = $pdo->prepare($sqlAnnoncesVentes);
$stmt_annonces_ventes->bindParam(':code_postal_annonces_ventes', $code_postal_annonces_ventes);
$stmt_annonces_ventes->execute();

$donnees = $stmt_annonces_ventes->fetchAll(PDO::FETCH_ASSOC);
$annonce = array();

$ListePrix = array();
$ListeNbrePiece = array();
$ListeSurface = array();

if(count($donnees) > 0){
	$i = 0;
	/**
	* x = prix
	* y = Superficie
	* z = Nb pièce
	*/ 
	foreach($donnees as $infos){
		if($infos['prix_annonces_ventes'] > 0 && $infos['nbre_piece_annonces_ventes'] > 0 && $infos['superficie_annonces_ventes'] > 0){
			if($infos['type_annonces_ventes'] == '1'){
				$annonce['infos'][$i]['name'] = 'Appartement';
			}elseif($infos['type_annonces_ventes'] == '2'){
				$annonce['infos'][$i]['name'] = 'Maison';
			}else{
				continue;
			}

			$annonce['infos'][$i]['x'] = (int)$infos['prix_annonces_ventes'];
			$annonce['infos'][$i]['y'] = (int)$infos['superficie_annonces_ventes'];
			$annonce['infos'][$i]['z'] = (int)$infos['nbre_piece_annonces_ventes'];
			$annonce['infos'][$i]['url'] = $infos['url_annonces_ventes'];
			
			$ListePrix[] = (int)$infos['prix_annonces_ventes'];
			$ListeNbrePiece[] = (int)$infos['nbre_piece_annonces_ventes'];
			$ListeSurface[] = (int)$infos['superficie_annonces_ventes'];
			$i++;
		}else{
			continue;
		}
	}
	
	$annonce['max'][$i+1]['MaxPrix'] = max($ListePrix);
	$annonce['max'][$i+1]['MaxPiece'] = max($ListeNbrePiece);
	$annonce['max'][$i+1]['MaxSurface'] = max($ListeSurface);
	
	$annonce['min'][$i+2]['MinPrix'] = min($ListePrix);
	$annonce['min'][$i+2]['MinPiece'] = min($ListeNbrePiece);
	$annonce['min'][$i+2]['MinSurface'] = min($ListeSurface);
	echo json_encode($annonce);
}