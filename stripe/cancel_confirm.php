<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}
include '../includes/sqlConnect.php';
include '../includes/fonction.php';

$abo = info_abonnement($_SESSION['email']);
$date_fin = new DateTime($abo->date_fin);
$date_fin = $date_fin->format('d/m/Y');
?>
<html>
	<head>
		<title>Confirmation de résiliation - <?= NOM_SITE;?></title>
		<?php include '../includes/meta.php';?>
		<script src="js/main.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
		<style>
		.navbar{
			margin-bottom:0px;
		}
		
		.search{
			margin-bottom:10px;
		}
		</style>

	</head>
	<body>
	
		<?php include '../includes/navbar.php';?>
	
		<div class="container">

		
			<div class="row">
				<div class="col-md-12">
				<h1>Résiliation confirmée</h1>
				<p>Votre résiliation a bien été effectuée et sera effective à la fin de votre période d'abonnement soit le <?= $date_fin;?></p>
				<p>Merci d'avoir utilisé notre solution</p>
				<p>Toute l'équipe d'immObserver</p>
				</div><!--/ col-md-12 -->
				
				
			</div><!-- row -->

		
		</div><!-- container -->
	</body>
</html>