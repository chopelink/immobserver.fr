<?php
session_start();
require_once('./config.php');
require_once('../includes/fonction.php');

ini_set('display_errors','on');
error_reporting(E_ALL);

// recup�ration info actuelle de l'abonnement
$abo = info_abonnement($_SESSION['email']);

// token
$token  = $_POST['stripeToken'];

// la formule
if($_POST['formule'] == 1){
	$nom_formule = 'immo-essentiel';
	$nb_recherche = 30;
	$nb_ville = 5;
	$nb_audit = 10;
}
else if($_POST['formule'] == 2){
	$nom_formule = 'immo-investisseur';
	$nb_recherche = 85;
	$nb_ville = 10;
	$nb_audit = 25;
}
else if($_POST['formule'] == 3){
	$nom_formule = 'immo-rentier';
	$nb_recherche = 200;
	$nb_ville = 25;
	$nb_audit = 50;
}
else if($_POST['formule'] == 4){
	$nom_formule = 'immo-platinium';
	$nb_recherche = 500;
	$nb_ville = 100;
	$nb_audit = 100;
}
else{
	echo 'erreur formule';
	exit;
}

// si un abonnement existe deja => on modifie l'abonnement
if(!empty($abo->id_stripe)){
	$abonnement = \Stripe\Subscription::retrieve($abo->id_stripe);
	
	\Stripe\Subscription::update($abo->id_stripe, [
		'items' => [
			[
				//'cancel_at_period_end' => false,
				'id' => $abonnement->items->data[0]->id,
				'plan' => $nom_formule,
			],
		],
	]);
	
}

// nouvel abonnement
else{
	// cr�ation du client
	$customer = \Stripe\Customer::create(array(
	  'email' => $_SESSION['email'],
	  'source'  => $token
	));

	// cr�ation abonnement OU update
	$abonnement = \Stripe\Subscription::create(array(
	  "customer" => $customer->id,
	  "items" => array(
		array(
		  "plan" => $nom_formule,
		),
	  ),
	));
}



// mise � jour de l'abonnement
include('../includes/sqlConnect.php');

$date_debut = new DateTime(date("Y-m-d H:i:s"));
$date_fin = new DateTime(date("Y-m-d H:i:s"));
$date_fin->add(new DateInterval('P1M')); //on ajoute 1 mois

$pdo->exec('UPDATE abonnement SET id_stripe = "'.$abonnement['id'].'", formule = "'.$nom_formule.'", nb_recherche = "'.$nb_recherche.'", nb_ville = "'.$nb_ville.'", nb_audit = "'.$nb_audit.'", date_debut = "'.$date_debut->format('Y-m-d H:i:s').'", date_fin = "'.$date_fin->format('Y-m-d H:i:s').'" WHERE mail = "'.$_SESSION['email'].'"');

// on redirige vers la page de remerciement
header('Location: ../merci.php');
exit;
/*echo var_dump($_POST);
echo'<pre>';
print_r($abonnement);
echo'</pre>';
echo '<p>ID = '.$abonnement['id'].'</p>';*/
?>
<h1>Abonnement r�ussi</h1>