<?php
session_start();
require_once('./config.php');
require_once('../includes/fonction.php');

ini_set('display_errors','on');
error_reporting(E_ALL);

// recupération info actuelle de l'abonnement
$abo = info_abonnement($_SESSION['email']);

$subscription = \Stripe\Subscription::retrieve($abo->id_stripe);
$subscription->cancel(['at_period_end' => true]);

// changement de l'abonnement chez stripe
$abonnement = \Stripe\Subscription::retrieve($abo->id_stripe);

\Stripe\Subscription::update($abo->id_stripe, [
	'items' => [
		[
			//'cancel_at_period_end' => false,
			'id' => $abonnement->items->data[0]->id,
			'plan' => 'immo-gratuit',
		],
	],
]);

// mise à jour de l'abonnement
include('../includes/sqlConnect.php');
$pdo->exec('UPDATE abonnement SET formule = "immo-gratuit" WHERE mail = "'.$_SESSION['email'].'"');

// on modifie la table ?
header ('Location: cancel_confirm.php');
exit();
?>