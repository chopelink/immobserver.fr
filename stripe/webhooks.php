<?php
session_start();
require_once('./config.php');
require_once('../includes/sqlConnect.php');

ini_set('display_errors','on');
error_reporting(E_ALL);

// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input");
$event_json = json_decode($input);

$event_id = $event_json->id;
$invoice_id = $event_json->data->object->id;
$abonnement_id = $event_json->data->object->subscription;

$formule_abonnement = $event_json->data->object->lines->data[0]->plan->id;
//$abonnement = \Stripe\Subscription::retrieve($abonnement_id);

$contenu = 'invoice = '.$invoice_id.' | abonnement = '.$abonnement_id.' | formule = '.$formule_abonnement.' ';

//$customer = Stripe_Customer::retrieve(event->data->object->customer);


// This will send receipts on succesful invoices
if ($event_json->type == 'invoice.payment_succeeded') {
	
	// en fonction des formules on recharge
	if($formule_abonnement == 'immo-essentiel'){
		$nb_recherche = 30;
		$nb_ville = 5;
		$nb_audit = 10;
	}
	else if($formule_abonnement == 'immo-investisseur'){
		$nb_recherche = 85;
		$nb_ville = 10;
		$nb_audit = 25;
	}
	else if($formule_abonnement == 'immo-rentier'){
		$nb_recherche = 200;
		$nb_ville = 25;
		$nb_audit = 50;
	}
	else if($formule_abonnement == 'immo-platinium'){
		$nb_recherche = 500;
		$nb_ville = 100;
		$nb_audit = 100;
	}
	else{
		echo 'erreur formule';
		exit;
	}
	
	// on update l'abonnement
	$requete = $pdo->prepare('UPDATE abonnement SET nb_recherche = :nb_recherche, nb_ville = :nb_ville, nb_audit = :nb_audit  WHERE id_stripe = :id_stripe');
	$requete->execute(array(
		'nb_recherche' => $nb_recherche,
		'nb_ville' => $nb_ville,
		'nb_audit' => $nb_audit,
		'id_stripe' => $abonnement_id
	));
	
	// envoi de mail
	
	
	// Do something with $event_json
	$file = fopen("fichier.txt", "a");
	fwrite($file,$contenu);
	fclose($file);
}




http_response_code(200); // PHP 5.4 or greater
?>