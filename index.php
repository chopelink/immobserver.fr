<?php
session_start();
include 'includes/fonction.php';

$nb_annonce = number_format(nb_annonce(), 0, ',', ' ');
$nb_recherche = number_format(nb_recherche(), 0, ',', ' ');

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Trouvez les meilleurs investissements immobiliers - <?= NOM_SITE;?></title>
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
	<?php include 'includes/meta.php';?>
	<meta name="google-site-verification" content="ReALmqb3fRHk0SGaHaAFSpPMAqtXXol11T6ACKNiIts" />
	<link rel="canonical" href="<?= BASE_URL;?>">
	<style>

	</style>
</head>
<body>

<?php include 'includes/navbar.php';?>

<!-- header -->
<div class="contenu">
	<h1>Les meilleurs investissements immobiliers</h1> 
	<span class="titre">Trouvez les meilleures biens immobiliers du marché</span>
	<br />
	<p>L'intelligence artificielle au service de votre recherche</p>
	<p><a href="register.php" class="btn btn-default btn-nav">Essayez gratuitement maintenant</a></p>
	<div class="text-center mockup"><!--<img src="https://fr.oncrawl.com/wp-content/uploads/2017/09/oncrawl-screen.png" class="img-responsive"/>--><img src="<?= BASE_URL;?>/images/immobserver-mac.png" class="img-responsive" /></div>
</div>
<!-- header -->

<!-- stat -->
<div class="row text-center bloc_stat">
	<div class="container">
		<div class="col-md-4">
			<div class="compteur"><?= $nb_annonce;?></div>
			<span>BIENS IMMOBILIERS ANALYSÉS</span>
		</div>
		<div class="col-md-4">
			<div class="compteur"><?= $nb_recherche;?></div>
			<span>RECHERCHES RÉALISÉES</span>
		</div>
		<div class="col-md-4">
			<div class="compteur">24%</div>
			<span>MEILLEUR RENDEMENT LOCATIF</span>
		</div>
	</div>
</div>
<!-- stat -->

<div class="container" id="fonctionnalites">
		<h2>Qu'est ce que ImmObserver ?</h2>
		<p class="text-contenu text-justify">Immobserver est un outil basé sur de l’intelligence artificielle parcourant internet pour vous <b>trouver les meilleures annonces immobilières du marché</b> ainsi que les plus rentables. L'investissement immobilier devient alors simple. Immobserver analyse des milliers d'annonces en temps réels et les classe selon vos critères : rendement locatif, prix au m², superficie, etc, tout cela en moins d’une minute.</p>
		
		<br />
		
		<div class="row">
			<div class="col-md-5">
				<img src="<?= BASE_URL;?>/images/screen-tool.jpg"  class="img-responsive" />
			</div>
			<div class="col-md-7">
				<h2>Trouvez les <strong>meilleures annonces du marché</strong><br /> en 5 secondes</h2>
				<p class="text-contenu text-justify">Trouver de bon investissement immobilier est fastidieux et long de nos jours. Immobserver vous propose de trouver des biens avec un <strong>rendement locatif de plus de 15%</strong> en <u>moins de 5 secondes</u>. Vous rentrez le nom de votre commune et le logiciel vous trouve les meilleures annonces. Nos algorithmes analysent des milliers d'annonces chaque jour pour ne garder que les meilleures, en prenant en compte <u>uniquement des critères objectifs</u> : taux de rendement locatif, prix au m², cash flow etc.</p>
				<p>Immobserver catégorise le type de bien et détermine automatiquement pour vous les opportunités d'investissement.</p>
				<!--<a href="" class="btn btn-default btn-simple">Découvrir les meilleurs biens immobiliers <span class="glyphicon glyphicon-chevron-right"></span></a>-->
			</div>
		</div><!-- row -->
		
		<hr />
		
		<div class="row">
			<div class="col-md-7">
				<h2>Enregistrez vos annonces</h2>
				<p>Il vous est possible à chacune de vos recherches d'enregistrer les annonces qui vous semble intèressantes, d'ajouter des notes durant votre visite et pouvoir revenir dessus quelques jours plus tard ou même retrouver tous les indicateurs.</p>
				<p>Toutes vos données sont accessibles que vous soyez chez vous, au travail ou même en déplacement.</p>
				<!--<div class="text-center">
					<img src="https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/computer-64.png" />
					<img src="https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/tablet-64.png" />
					<img src="https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/smartphone-64.png" />
				</div>-->
			</div>
			<div class="col-md-5">
				<img src="<?= BASE_URL;?>/images/save-annonce.jpg" alt="sauvegarde annonce immobilière" class="img-responsive" />
			</div>
		</div><!-- row -->
		
		<hr />
		
		<div class="row">
			<div class="col-md-5">
				<img src="https://cdn1.iconfinder.com/data/icons/flat-business-icons/128/chart-256.png"  class="img-responsive" />
				
			</div>
			<div class="col-md-7">
				<h2>Appuyez vos décisions d'investissement sur des <strong>statistiques précises en temps réel</strong></h2>
				<p>Nous avons développé des algorithmes capable d'analyser des milliers d'annonces immobilières ainsi qu'un trés grand nombre de données pour vous aider dans votre choix d'investissement.</p>
				<p>A disposition pour <u>plus de 35 000 communes</u> :</p>
				<ul class="accueil">
					<li>Prix au m² moyen (distinction par type de bien : maison ou appartement)en temps réel</li>
					<li>Calcul des mensualités en fonction des taux immobilier actuel : sur 7 à 30 ans</li>
					<li>Calcul <b>taux de rendement locatif</b> brut</li>
					<li>Calcul du <b>cashflow brut</b</li>
				</ul>
			</div>
		</div><!-- row -->
		
	
		
</div><!-- container -->

<div class="bandeau">
	<div class="container text-center">
	<h2>Trouvez des maintenant<br /> les <b>meilleurs investissements immobiliers</b><br /> à faire près de chez vous</h2>
	<a href="register.php" class="btn btn-default cta cta-accueil" style="background-color:#46BAA7;border:1px solid #46BAA7;">Je commence maintenant !</a>
	</div>
</div>

<!-- footer -->
<?php include 'includes/footer.php';?>

</body>
</html>