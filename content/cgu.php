<?php
session_start();
include '../includes/fonction.php';

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Conditions générales d'utilisation - <?= NOM_SITE;?></title>
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
	<?php include '../includes/meta.php';?>
	<link rel="canonical" href="">
</head>
<body>

<?php include '../includes/navbar.php';?>

<!-- stat -->
<div class="row text-center bloc_stat">
	<div class="container">
		<h1>Conditions générales d'utilisation</h1>
	</div>
</div>
<!-- stat -->

<div class="container">

		<div class="row">
			<p>En cours de rédaction</p>
			
			<h3>Définitions :</h3>
			<b>Outil :</b> les services d'analyse immobilier
			
			
			<p>Les présentes conditions générales ont pour objet de définir les modalités et conditions d’utilisation de l'Outil , ainsi que de définir les droits et obligations des parties dans ce cadre.</p>
			
			<h3>Prix :</h3>
			Le prix des Services est indiqué sur le site.
Sauf mention contraire, ils sont exprimés en euros et toutes taxes comprises.
			
			<h3>Paiement :</h3>
			Les modalités de paiement du prix des Services sont décrites sur le site.
Le paiement s’effectue par prélèvement automatique à partir du numéro de carte bancaire de l’Utilisateur.
Le prélèvement est mis en œuvre par le prestataire de paiement désigné sur le site, qui seul conserve les coordonnées bancaires de l’Utilisateur à cette fin. L’Éditeur ne conserve aucune coordonnée bancaire.
			
			
			<h3>Article 22 : Résiliation</h3>
			L'utilisateur et/ou client d'Immobserver peut résilier son abonnement à tout moment sur son compte 
		
		</div>

	

		
</div><!-- container -->

<div class="bandeau">
	<div class="container text-center">
	<h2>Trouvez des maintenant<br /> les <b>meilleurs investissements immobiliers</b><br /> à faire près de chez vous</h2>
	<a href="register.php" class="btn btn-default cta cta-accueil" style="background-color:#46BAA7;border:1px solid #46BAA7;">Je commence maintenant !</a>
	</div>
</div>

<!-- footer -->
<?php include '../includes/footer.php';?>

</body>
</html>