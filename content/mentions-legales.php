<?php
session_start();
include '../includes/fonction.php';

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Mentions légales - <?= NOM_SITE;?></title>
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
	<?php include '../includes/meta.php';?>
	<link rel="canonical" href="">
</head>
<body>

<?php include '../includes/navbar.php';?>

<!-- stat -->
<div class="row text-center bloc_stat">
	<div class="container">
		<h1>Mentions légales</h1>
	</div>
</div>
<!-- stat -->

<div class="container">

		<div class="row">
		
		<p><u>Immobserver</u><br />
<b>Code APE :</b> Programmation informatique (6201Z)</p>

<p>Directeur de la publication : Florian Vaqué<br />
Fondateur : Florian Vaqué</p>

	<p>Siège social :<br />
	Chemin de St Pierre<br />
	07400 Le Teil<br />
	France</p>

	<p><u>Hébergement</u> :<br />
	SAS au capital de 10 069 020 €<br />
	RCS Lille Métropole 424 761 419 00045<br />
	Code APE 2620Z<br />
	N° TVA : FR 22 424 761 419<br />
	Siège social : 2 rue Kellermann - 59100 Roubaix - France.<br />
	Directeur de la publication : Octave KLABA</p>


	<p><i>Conformément à la loi 78-17 du 6 Janvier 1978 relative à l'informatique, aux fichiers et aux libertés, l'internaute est informé de ce que les données nominatives ou de caractère personnel signalées comme étant obligatoires et recueillies dans le cadre de l'utilisation du service sont nécessaires à l'utilisation de celui-ci. Ces informations sont destinées principalement à la société Immobserver. L’internaute bénéficie d'un droit d'accès, de rectification et d'opposition à la cession de ces données qu'il peut exercer en s'adressant à Florian Vaqué, Quartier St Pierre, 07400 Le teil.
	Par ailleurs, comme la plupart des sites internet, nous collectons également des données relatives à la navigation de nos visiteurs, afin d'obtenir des statistiques précises liées à l'utilisation de notre site. Ces données incluent l’adresse IP, le fournisseur d'accès à internet, le navigateur utilisé, ou encore l'heure des visites.
	Chaque internaute a la possibilité de désactiver les cookies et d'effacer ceux présents sur son ordinateur, en modifiant les réglages de son navigateur.</i></p>
</div>

	

		
</div><!-- container -->

<div class="bandeau">
	<div class="container text-center">
	<h2>Trouvez des maintenant<br /> les <b>meilleurs investissements immobiliers</b><br /> à faire près de chez vous</h2>
	<a href="register.php" class="btn btn-default cta cta-accueil" style="background-color:#46BAA7;border:1px solid #46BAA7;">Je commence maintenant !</a>
	</div>
</div>

<!-- footer -->
<?php include '../includes/footer.php';?>

</body>
</html>