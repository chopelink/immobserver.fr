<?php
session_start();
include 'includes/fonction.php';
include 'includes/sqlConnect.php';

if(isset($_POST['register'])){
	
	$syntaxe = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
	
	//Verif Mail
	if(!preg_match($syntaxe,$_POST['email'])){
		$msg = '<div class="alert alert-danger">Le format de votre <strong>adresse email</strong> est mauvais.</div>';
	}
	else{
		$email = $_POST['email'];
		$pass = $_POST['pass'];
		$prenom = $_POST['prenom'];
		$date_register = date("Y-m-d H:i:s");
		$date_last_login = date("Y-m-d H:i:s");
		$adress_ip = $_SERVER['REMOTE_ADDR'];
		
		$req = $pdo->query('SELECT count(id) FROM membre WHERE email = "'.$email.'"');
		$data = $req->fetch();
		$req->closeCursor();

			if ($data[0] == 0) {
				/* Enregistrement du membre */
				$requete = $pdo->prepare('INSERT INTO membre(email,pass,prenom,date_register,date_last_login,adress_ip) VALUES(:email,:pass,:prenom,:date_register,:date_last_login,:adress_ip)');
				$requete->execute(array(
				'email' => $email,
				'pass' => sha1($pass),
				'prenom' => $prenom,
				'date_register' => $date_register,
				'date_last_login' => $date_last_login,
				'adress_ip' => $adress_ip
				));

				$_SESSION['email'] = $email;
				$_SESSION['prenom'] = $prenom;
				
				/* Création abonnement */
				$date_debut = new DateTime(date("Y-m-d H:i:s"));
				$date_fin = new DateTime(date("Y-m-d H:i:s"));
				$date_fin->add(new DateInterval('P1M')); //on ajoute 1 mois

				
				$req = $pdo->prepare('INSERT INTO abonnement(mail,id_stripe,formule,date_debut,date_fin,nb_recherche,nb_ville,nb_audit) VALUES(:mail,:id_stripe,:formule,:date_debut,:date_fin,:nb_recherche,:nb_ville,:nb_audit)');
				$req->execute(array(
				'mail' => $email,
				'id_stripe' => '',
				'formule' => 'immo-gratuit',
				'date_debut' => $date_debut->format('Y-m-d H:i:s'),
				'date_fin' => $date_fin->format('Y-m-d H:i:s'),
				'nb_recherche' => 3,
				'nb_ville' => 1,
				'nb_audit' => 1
				));
				
				header('Location: search.php');
				exit;
			}
			else{
				$msg = '<div class="alert alert-warning">Un compte existe déja avec cette adresse email.</div>';
			}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Créer mon compte - <?= NOM_SITE;?></title>

	<?php include 'includes/meta.php';?>
	<style>
	body{
		background-color:#eee;
	}
	
	.logo{
		margin-top:20px;
		margin-bottom:20px;
	}
	
	.btn-nav{
		border-color:#e04f5f;
		font-size:18px;
		padding:10px;
	}
	
	.form-control{
		border-radius:0px;
		padding-top:15px;
		padding-bottom:15px;
		box-shadow:none;
		height:40px;
	}
	</style>
</head>
<body>



	<div class="container">
	<div class="text-center mockup"><a href="<?= BASE_URL;?>"><img src="<?= BASE_URL;?>/images/immobserver-register.png" class="logo img-responsive" alt="logo immobserver"/></a></div>
		<?php if(isset($msg)){echo $msg;}?>
		<h1 class="text-center">Créez votre compte Gratuitement</h1>
		<p class="text-center">Trouvez des maintenant des biens immobiliers avec <b>15% de rentabilité</b></p>
		
		<div class="row">
		<div class="col-md-4 col-md-offset-4">
			
			<form method="POST" action="register.php">
				<div class="form-group">
					<label>Prénom</label>
					<input type="text" name="prenom" value="<?php if(isset($_POST['prenom'])){echo htmlspecialchars($_POST['prenom']);};?>" class="form-control" placeholder="Marie,Julien, ...">
				</div>
				
				<div class="form-group">
					<label>Email</label>
					<input type="text" name="email" value="<?php if(isset($_POST['email'])){echo htmlspecialchars($_POST['email']);};?>" class="form-control" placeholder="adresse@email.com">
				</div>
				
				<div class="form-group">
					<label>Mot de passe</label>
					<input type="password" name="pass" class="form-control" placeholder="*****">
				</div>
				
				<div class="form-group text-right">
					<button type="submit" name="register" class="btn btn-default btn-nav">Créer mon compte</button>
				</div>
			</form>
		</div>
		</div><!-- row -->
		

		<p class="text-center">J'ai déja un compte - <a href="<?= BASE_URL;?>/login.php">Me connecter</a></p>

		
	</div><!-- container -->
	

</body>
</html>