﻿<?php
header("Content-type: text/html; charset=utf-8");

include 'includes/sqlConnect.php';
include 'includes/fonction.php';
include 'includes/scraping.php';
include 'includes/scraping_seloger.php';
include 'includes/scraping_paruvendu.php';
include 'includes/scraping_foncia.php';
include 'includes/scraping_safti.php';
include 'includes/arrondissement.php';

$donnees = array();
if(isset($_GET['code_postal']) && !empty($_GET['code_postal'])){
	$code_postal_annonces_ventes = $_GET['code_postal'];
	$code_insee_annonces_ventes = $_GET['code_insee'];
}else{
	exit(json_encode($donnees));
}

//Affichage des annonces depuis 7 jours
$date_debut = date('Y-m-d',mktime(0,0,0,(int)date('n'),(int)date('j')-7,date('Y')));
$date_fin = date('Y-m-d',mktime(0,0,0,(int)date('n'),(int)date('j'),date('Y')));

$sqlAnnoncesVentes = "SELECT 
						* 
						FROM annonces_ventes 
						WHERE code_postal_annonces_ventes = :code_postal_annonces_ventes 
						AND code_insee_annonces_ventes = :code_insee_annonces_ventes
						AND (DATE(date_scrap_annonces_ventes) BETWEEN :date_debut AND :date_fin)
						AND LOWER(titre_annonces_ventes) NOT LIKE '%immeub%'
						GROUP BY url_annonces_ventes
					";
$stmt_annonces_ventes = $pdo->prepare($sqlAnnoncesVentes);
$stmt_annonces_ventes->bindParam(':code_postal_annonces_ventes', $code_postal_annonces_ventes);
$stmt_annonces_ventes->bindParam(':code_insee_annonces_ventes', $code_insee_annonces_ventes);
$stmt_annonces_ventes->bindParam(':date_debut', $date_debut);
$stmt_annonces_ventes->bindParam(':date_fin', $date_fin);
$stmt_annonces_ventes->execute();

$donnees = $stmt_annonces_ventes->fetchAll(PDO::FETCH_ASSOC);
$annonce = array();

$nb_maison = 0;
$nb_appartement = 0;
if(count($donnees) > 0){
	$i = 0;
	foreach($donnees as $infos){
		//maisons
		if($infos['type_annonces_ventes'] == '2' && $infos['prix_annonces_ventes'] > 0){
			$nb_maison++;
		}
		//Appartements
		elseif($infos['type_annonces_ventes'] == '1' && $infos['prix_annonces_ventes'] > 0){
			$nb_appartement++;
		}
		else{
			continue;
		}
		$i++;
	}
	$annonce['Task'] = 'Répartitions des biens';
	$annonce['Maisons'] = $nb_maison;
	$annonce['Appartements'] = $nb_appartement;
}

echo json_encode($annonce);