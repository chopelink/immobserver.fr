<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
        error_reporting(E_ALL);ini_set('display_errors','on');
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $password) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(name, email, password_hash, api_key, status) values(?, ?, ?, ?, 1)");
            $stmt->bind_param("ssss", $name, $email, $password_hash, $api_key);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT name, email, api_key, status, created_at FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $status, $created_at);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        /*$stmt = $this->conn->prepare("SELECT id FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }*/
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT api_key from api WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `tasks` table method ------------------ */

    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
    public function createTask($user_id, $task) {
        $stmt = $this->conn->prepare("INSERT INTO tasks(task) VALUES(?)");
        $stmt->bind_param("s", $task);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
            $res = $this->createUserTask($user_id, $new_task_id);
            if ($res) {
                // task created successfully
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }
    }

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $task, $status, $created_at);
            // TODO
            // $task = $stmt->get_result()->fetch_assoc();
            $stmt->fetch();
            $res["id"] = $id;
            $res["task"] = $task;
            $res["status"] = $status;
            $res["created_at"] = $created_at;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    /*public function getAllUserTasks($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }*/

    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }

    public function getCodeInsee($codePostal)
    {
        $result = $this->conn->query("SELECT code_insee from liste_commune WHERE code_postal = '".$codePostal."' ");
        $code_insee = "";
        if(!$result)
        {
            return false;
        }
        while ( $rows = $result->fetch_assoc() ) 
        {
            //echo "{$row['field']}";
            $code_insee = $rows['code_insee'];


        }
        return $code_insee;
    }

    

    public function insertVille($array_ville) {

        $array_ville = $array_ville[0];
        $ville = mysqli_real_escape_string($this->conn,$array_ville['ville']);
        $code_insee = $array_ville['code_insee'];
        $code_postal = $array_ville['code_postal'];
        $titre = $array_ville['titre'];
        $type = $array_ville['type'];
        $prix = $array_ville['prix'];
        $m2 = $array_ville['m2'];
        $nb_piece = $array_ville['nb_piece'];
        $rendement_locatif = $array_ville['rendement_locatif'];
        $url = $array_ville['url'];
        $referrer = $array_ville['referrer'];
        $date = $array_ville['date'];
        $nb_chambre_annonces_ventes = -1;
        
        
        if(isset($array_ville['nb_chambre']))
        {
            
            $nb = (int)$array_ville['nb_chambre'];
            if($array_ville['date'] > 0)
            {
                $nb_chambre_annonces_ventes = $nb;
            }
            $nb_chambre_annonces_ventes = -1;
        }
        
        //$stmt = $this->conn->query("INSERT INTO annonces_ventes(titre_annonces_ventes) values('".$titre."')");
        //$url= "hehehee";
        $test_annonce_dupl = $this->conn->query("SELECT * from extension_annonces_ventes WHERE url_annonces_ventes = '".$url."'");
        //echo "SELECT id_annonces_ventes from extension_annonces_ventes WHERE url_annonces_ventes = '".$url."'";
        //echo "test=".$test_annonce_dupl;
        //echo "test=".sizeof($test_annonce_dupl)."...";
        //var_dump($test_annonce_dupl[0]['ville_annonces_ventes']);
        $test_dupl = false;
        foreach ($test_annonce_dupl as $key => $value) {
            # code...
            $test_dupl = true;
            /*echo "key=".$key;
            echo "<pre>";
            print_r($value);
            echo "</pre>";
            echo "</br>";*/
        }
        /*echo "<pre>";
        print_r($test_annonce_dupl['[num_rows]']);
        echo "</pre>";*/

        if($test_dupl == true)
        {
            return false;
        }
        else
        {
            $stmt = $this->conn->query("INSERT INTO extension_annonces_ventes (`titre_annonces_ventes`,`code_postal_annonces_ventes`, `code_insee_annonces_ventes`, `ville_annonces_ventes`, `type_annonces_ventes`, `prix_annonces_ventes`, `nbre_piece_annonces_ventes`, `nb_chambre_annonces_ventes`, `superficie_annonces_ventes`, `url_annonces_ventes`, `referrer_annonces_ventes`, `rendement_location_annonces_ventes`, `date_scrap_annonces_ventes`) VALUES ('$titre', '$code_postal', '$code_insee', '$ville', '$type', '$prix', '$nb_piece', '$nb_chambre_annonces_ventes', '$m2', '$url', '$referrer', '$rendement_locatif', '$date')");
            
            //$stmt->bind_param("i", $titre);
            $result = $stmt;
        }
        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        //$stmt->close();
       
        return $result;
    }


    public function getTauxImmobilier() 
    {
        /*$stmt = $this->conn->prepare("SELECT taux_immobilier from taux_imm WHERE id = ?");
        $result = $stmt->bind_param("s", $id);
        print_r($result);
        $stmt->execute();
        $stmt->store_result();

        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;*/


        $result = $this->conn->query("SELECT 20ans_marche from taux_immobilier WHERE 1 = 1 ");
        $taux = "";
        if(!$result)
        {
            return false;
        }
        while ( $rows = $result->fetch_assoc() ) 
        {
            //echo "{$row['field']}";
            $taux = $rows['20ans_marche'];


        }
        return $taux;
        //echo $taux;
        //print_r($taux);
    }

    public function getId($ville) 
    {
        /*$stmt = $this->conn->prepare("SELECT taux_immobilier from taux_imm WHERE id = ?");
        $result = $stmt->bind_param("s", $id);
        print_r($result);
        $stmt->execute();
        $stmt->store_result();

        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;*/
        $result = $this->conn->query("SELECT id from annonces WHERE ville = '".$ville."'");
        $id = "";
        if(!$result)
        {
            return false;
        }
        while ( $rows = $result->fetch_assoc() ) 
        {
            //echo "{$row['field']}";
            $id = $rows['id'];


        }
        
        return $id;
        //echo $taux;
       


    }

    public function getPrixm2AverageLocatif($ville) 
    {
        /*$stmt = $this->conn->prepare("SELECT taux_immobilier from taux_imm WHERE id = ?");
        $result = $stmt->bind_param("s", $id);
        print_r($result);
        $stmt->execute();
        $stmt->store_result();

        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;*/



        //$ville = iconv('utf-8', 'ascii//TRANSLIT', $ville);
        
        //$ville = strtoupper($ville);
        //echo "ville=".iconv("UTF-8", "ISO-8859-1//TRANSLIT", $ville);
        
        $result = $this->conn->query("SELECT AVG(prix_annonces_locations/superficie_annonces_locations) AS averagePrice FROM annonces_locations WHERE ville_annonces_locations = '".$ville."' ");
        
        /*echo "<pre>";
            print_r($result);
        echo "</pre>";*/
        $average = "";
        if(!$result)
        {
           
            return false;
        }

        
        while ( $rows = $result->fetch_assoc() ) 
        {
            //echo "{$row['field']}";
            //print_r($rows);
            //echo "row=".$rows['averagePrice'];
            $average = $rows['averagePrice'];


        }
        //echo "average=";
        //echo $average;
        
        /*$result = $this->conn->query("SELECT m2 from annonces WHERE id = '".$id."'");
        $m2 = "";
        if(!$result)
        {
            return false;
        }
        while ( $rows = $result->fetch_assoc() ) 
        {
            //echo "{$row['field']}";
            $m2 = $rows['m2'];


        }*/
        return $average;
        //return null;
        //echo $taux;
       


    }


    

    
}
?>
