<?php
/**
 * Database configuration
 */
define('DB_USERNAME', 'immobserver');
define('DB_PASSWORD', 'immobserver');
define('DB_HOST', 'localhost');
define('DB_NAME', 'immobserver');

define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);
?>
