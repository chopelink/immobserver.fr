<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}
include 'includes/sqlConnect.php';
include 'includes/fonction.php';
?>
<html>
	<head>
		<title>Mes recherches - <?= NOM_SITE;?></title>
		<?php include 'includes/meta.php';?>
		<script src="js/main.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
			<style>
		.corps{
			min-height:70%;
		}
		</style>
	</head>
	<body>
	
		<?php include 'includes/navbar.php';?>
	
		<div class="container">

		
			<div class="row corps">
				<div class="col-md-12">
				<h1>Mes dernières recherches</h1>
				<table class="table">
					<tr>
						<th>Ville</th>
						<th>Nb annonce</th>
						<th>Date de la recherche</th>
					</tr>
				<?php
				$req = $pdo->query('SELECT ville, code_postal, nb_annonce_find, DATE_FORMAT(date, "%d/%m/%Y à %Hh%i") AS date FROM recherche WHERE email = "'.$_SESSION['email'].'" ORDER BY id DESC');
					while($data = $req->fetch()){
						echo '<tr>';
						echo '<td>'.$data['ville'].' ('.$data['code_postal'].')</td>';
						echo '<td>'.$data['nb_annonce_find'].' bien(s)</td>';
						echo '<td>'.$data['date'].'</td>';
						echo '</tr>';
					}
				?>
				</table>
				</div><!--/ col-md-12 -->
				
			</div><!-- row -->

		
		</div><!-- container -->
		
		<?php include 'includes/footer.php';?>
	</body>
</html>