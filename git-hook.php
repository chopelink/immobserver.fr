<?php
// Set up a webhook for your Bitbucket repo (or other service) that will visit this script (eg, http://example.org/git-pull.php).
// When you commit, Bitbucket will visit that URL.
// Now the script below runs, which will then pull from the bitbucket repo, grabbing the recently uploaded commit.
// All you have to do is push to Bitbucket!
// NOTE: This script relies on shell_exec and may not work on shared hosts.
$LOCAL_REPO         = "/var/www/html/dev.immobserver.fr";
$REMOTE_REPO        = "https://andrantsa:antsa@010782@https://andrantsa@bitbucket.org/chopelink/immobserver.fr.git";
$output = shell_exec("cd {$LOCAL_REPO} && git pull {$REMOTE_REPO}");
echo '<pre>';
print_r($output);
echo '</pre>';