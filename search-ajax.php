<?php
session_start();

include 'includes/sqlConnect.php';
include 'includes/fonction.php';
include 'includes/scraping.php';
include 'includes/scraping_seloger.php';
include 'includes/scraping_paruvendu.php';
include 'includes/scraping_foncia.php';
include 'includes/scraping_safti.php';
include 'includes/scraping_orpi.php';
include 'includes/arrondissement.php';

// si l'abonnement à immobserver n'est plus bon : nb recherche = 0 ou date de fin dépassée => on redirige vers abonnement.php
$abo = info_abonnement($_SESSION['email']);
if(($abo->nb_recherche <= 0) OR ($abo->date_fin < date('Y-m-d H:i:s'))){
	echo '<script>window.location.href="abonnement.php"</script>';
	exit();
}


if(isset($_GET['ville']) AND isset($_GET['code_insee']) AND isset($_GET['code_postal'])){
	
	$annonces = array();
	
	// récupération des taux immobilier
	$taux = tx_immobilier();
	
	// taux de l'assurance du crédit
	$taux_assurance = 0.36;

	$city = explode ('(',$_GET['ville']);
	$ville = trim($city[0]);
	$code_insee = $_GET['code_insee'];
	$code_postal = $_GET['code_postal'];
	$date = date("Y-m-d H:i:s");
	$date_rand = $_GET['url_progress'];

	$filtres = array();
	$filtres['ci'] = $code_insee;
	$filtres['cp'] = $code_postal;
	$filtres['ville'] = $ville;
	$filtres['url_progress'] = $_SERVER['DOCUMENT_ROOT'].'/log/progress_'.$code_postal.'_'.$date_rand.'.txt';
	
	$compteurs = 0;
	
	//Affichage des annonces depuis 7 jours
	$date_debut = date('Y-m-d',mktime(0,0,0,(int)date('n'),(int)date('j')-7,date('Y')));
	$date_fin = date('Y-m-d',mktime(0,0,0,(int)date('n'),(int)date('j'),date('Y')));
	
	$liste_annonce = affiche_annonces($pdo, $filtres, 0, $date_debut, $date_fin);
	$compteurs++;
	
	// date du dernier scrap pour la ville => permet de connaitre la fraicheur des annonces
	$Nbre = count($liste_annonce);
	$date_last_scrap = new DateTime($liste_annonce[0]['date']);
	$date_last_scrap = $date_last_scrap->format('d/m/Y à H:i:s');
	

	
	ecrire_fichier_progression($filtres['url_progress'], 'Affichage des annonces en cours ...#'.$compteurs.'');


	$nb_annonce_find = count($liste_annonce);
	$adresseIp = recupere_ip();
	$cookie = '';

	//insertion dans la base de la recherche
	$requete = $pdo->prepare('INSERT INTO recherche(ville,code_insee,code_postal,nb_annonce_find,ip,cookie,email,date) VALUES(:ville,:code_insee,:code_postal,:nb_annonce_find,:ip,:cookie,:email,:date)');
	$requete->execute(array(
		'ville' => $ville,
		'code_insee' => $code_insee,
		'code_postal' => $code_postal,
		'nb_annonce_find' => $nb_annonce_find,
		'ip' => $adresseIp,
		'cookie' => $cookie,
		'email' => $_SESSION['email'],
		'date' => $date
	));

	//mise à jour du nombre de recherche dans l'abonnement
	$requete = $pdo->prepare('UPDATE abonnement SET nb_recherche = nb_recherche-1 WHERE mail = :mail');
	$requete->execute(array(
		'mail' => $_SESSION['email']
	));

	/* calcul statistiques sur le marché immobilier de la ville */
	$liste_prix_appartement = array();
	$liste_prix_maison = array();
	
	$liste_m2_appartement = array();
	$liste_m2_maison = array();
	
	$liste_prix_m2_appartement = array();
	$liste_prix_m2_maison = array();
	
	$indice = 1;
	foreach($liste_annonce as $element){
		//eneleve les ligne ou le prix et les m2 ne sont pas bons
		if(is_numeric($element['prix']) AND $element['prix'] > 0 AND is_numeric($element['surface']) AND $element['surface'] > 0 ){
			//Appartement
			if($element['type'] == '1'){
				$liste_prix_appartement[] = $element['prix'];
				$liste_m2_appartement[] = $element['surface'];
				$liste_prix_m2_appartement[] = round($element['prix']/$element['surface']);
			}
			//Maison
			elseif($element['type'] == '2'){
				$liste_prix_maison[] = $element['prix'];
				$liste_m2_maison[] = $element['surface'];
				$liste_prix_m2_maison[] = round($element['prix']/$element['surface']);
			}
		}
		$indice++;
	}

	$max_prix_appartement = (isset($liste_prix_appartement) && count($liste_prix_appartement) > 0) ? max($liste_prix_appartement) : 0;
	$min_prix_appartement = (isset($liste_prix_appartement) && count($liste_prix_appartement) > 0) ? min($liste_prix_appartement) : 0;
	
	$max_prix_maison = (isset($liste_prix_maison) && count($liste_prix_maison) > 0) ? max($liste_prix_maison) : 0;
	$min_prix_maison = (isset($liste_prix_maison) && count($liste_prix_maison) > 0) ? min($liste_prix_maison) : 0;

	
	$max_m2_appartement = (isset($liste_m2_appartement) && count($liste_m2_appartement) > 0) ? max($liste_m2_appartement) : 0;
	$min_m2_appartement = (isset($liste_m2_appartement) && count($liste_m2_appartement) > 0) ? min($liste_m2_appartement) : 0;
	
	$max_m2_maison = (isset($liste_m2_maison) && count($liste_m2_maison) > 0) ? max($liste_m2_maison) : 0;
	$min_m2_maison = (isset($liste_m2_maison) && count($liste_m2_maison) > 0) ? min($liste_m2_maison) : 0;

	$max_prix_m2_appartement = (isset($liste_prix_m2_appartement) && count($liste_prix_m2_appartement) > 0) ? max($liste_prix_m2_appartement) : 0;
	$min_prix_m2_appartement = (isset($liste_prix_m2_appartement) && count($liste_prix_m2_appartement) > 0) ? min($liste_prix_m2_appartement) : 0;
	
	$max_prix_m2_maison = (isset($liste_prix_m2_maison) && count($liste_prix_m2_maison) > 0) ? max($liste_prix_m2_maison) : 0;
	$min_prix_m2_maison = (isset($liste_prix_m2_maison) && count($liste_prix_m2_maison) > 0) ? min($liste_prix_m2_maison) : 0;

	if(count($liste_prix_m2_appartement) > 0){
		$moyenne_prix_m2_appartement = array_sum($liste_prix_m2_appartement)/count($liste_prix_m2_appartement);
	}else{
		$moyenne_prix_m2_appartement = 0;
	}
	
	if(count($liste_prix_m2_maison) > 0){
		$moyenne_prix_m2_maison = array_sum($liste_prix_m2_maison)/count($liste_prix_m2_maison);
	}else{
		$moyenne_prix_m2_maison = 0;
	}
	
	$prix_m2_locatif = array();
	//Apparatement
	$prix_m2_locatif_appartement = 0;
	$prix_m2_locatif = recuperer_annonces_prix_mettre_carrer_historique($pdo, array('ci' => $code_insee, 'cp' => $code_postal), 1, $date_debut, $date_fin);
	if(count($prix_m2_locatif) <= 0){
		$prix_m2_locatif = recuperer_annonces_prix_mettre_carrer_historique($pdo, array('ci' => $code_insee, 'cp' => $code_postal), 2, $date_debut, $date_fin);
	}
	
	//Moyenne des prix au mettre carrée appartements
	$cpt_apprt = 0;
	$somme_prix_m2_locatif_appartement = 0;
	foreach($prix_m2_locatif as $m2_locatif){
		$somme_prix_m2_locatif_appartement = $somme_prix_m2_locatif_appartement + $m2_locatif['valeur_annonces_prix_mettre_carre_historique'];
		$cpt_apprt++;
	}
	$prix_m2_locatif_appartement = round($somme_prix_m2_locatif_appartement/$cpt_apprt, 2);
	
	//Maison
	$prix_m2_locatif_maison = 0;
	$prix_m2_locatif = recuperer_annonces_prix_mettre_carrer_historique($pdo, array('ci' => $code_insee, 'cp' => $code_postal), 2, $date_debut, $date_fin);
	if(count($prix_m2_locatif) <= 0){
		$prix_m2_locatif = recuperer_annonces_prix_mettre_carrer_historique($pdo, array('ci' => $code_insee, 'cp' => $code_postal), 1, $date_debut, $date_fin);
	}
	//Moyenne des prix au mettre carrée maisons
	$cpt_maison = 0;
	$somme_prix_m2_locatif_maison = 0;
	foreach($prix_m2_locatif as $m2_locatif){
		$somme_prix_m2_locatif_maison = $somme_prix_m2_locatif_maison + $m2_locatif['valeur_annonces_prix_mettre_carre_historique'];
		$cpt_maison++;
	}
	$prix_m2_locatif_maison = round($somme_prix_m2_locatif_maison/$cpt_maison, 2);
?>
	<div class="row">
		<div class="col-md-9">
			<div class="reponse"></div>

			<div class="panel panel-default">
				<div class="panel-heading">
					Liste des meilleures annonces sur <?= $ville;?> (<?= $code_postal;?>) : <span class="label label-success pull-right"><?= $nb_annonce_find;?> annonces</span>
				</div>

				<table id="example" class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Type</th>
							<th>Titre de l'annonce</th>
							<th>Nb pièce</th>
							<th>Prix</th>
							<th>Superficie</th>
							<th>Prix au m²<br/ ><small>vs moy. ville</small></th>
							<th class="text-center">Rendement locatif<br /> Brut</th>
							<th>Cashflow Brut</th>
							<th>Action</th>
						</tr>
					</thead>
<?php
					foreach($liste_annonce as $annonce){
						
						
						
						$compteurs++;
						
						$prix_m2_moyen_appart = 0;
						$prix_m2_moyen_maison = 0;
						$prix_m2_moyen_appart = $prix_m2_locatif_appartement;
						$prix_m2_moyen_maison = $prix_m2_locatif_maison;
	
						if($annonce['surface'] > 0){
							$prix_m2 = round($annonce['prix']/$annonce['surface']);
						}else{
							$prix_m2 = 0;
						}
						
						if($prix_m2 <= 0){
							continue;
						}
						
						$rendement_locatif_appartement = 0;
						$rendement_locatif_maison = 0;
						if($annonce['prix'] > 0){
							$rendement_locatif_appartement = round((($annonce['surface']*$prix_m2_locatif_appartement)*12)/$annonce['prix'],2)*100;
							$rendement_locatif_maison = round((($annonce['surface']*$prix_m2_locatif_maison)*12)/$annonce['prix'],2)*100;
						}

						//mise en couleur des meilleures offres
						echo '<tr>';
						//echo '<td>'.$annonce['referrer'].'</td>';
						echo '<td>'.type_bien($annonce['type']).'</td>';
						echo '<td>'.$annonce['titre'].'</td>';

						//si nb pièce = 0 c'est que l'information ne peut être récupérée
						if($annonce['nbre_piece'] == 0){
							echo '<td>NC</td>';
						}
						else{
							echo '<td>'.$annonce['nbre_piece'].'</td>';
						}

						echo '<td>'.number_format($annonce['prix'], 0, ',', ' ').'€</td>';
						echo '<td class="text-center">'.round($annonce['surface']).' m²</td>';

						echo '<td class="text-center">'.number_format($prix_m2, 0, ',', ' ').'€';
						
						if($annonce['type'] == '1'){
							$diff = round((($prix_m2 - $moyenne_prix_m2_appartement)/$moyenne_prix_m2_appartement)*100);
						}elseif($annonce['type'] == '2'){
							$diff = round((($prix_m2 - $moyenne_prix_m2_maison)/$moyenne_prix_m2_maison)*100);
						}
						
						
						if($diff < 0){
							echo ' <sup><span class="text-success"><b>'.$diff.'%</b></span></sup>';
						}
						else{
							echo ' <sup><span class="text-danger"><b>+'.$diff.'%</b></span></sup>';
						}
						echo '</td>';

						echo '<td class="text-center">';
						
						if($annonce['type'] == '1'){
							//rentabilité locative supérieur à 7
							if($rendement_locatif_appartement > 7){
								echo '<span class="label label-success">'.$rendement_locatif_appartement.' %</span> ';
							}
							//rentabilité locative entre 4 et 8
							else if($rendement_locatif_appartement <= 8 AND $rendement_locatif_appartement >= 4){
								echo '<span class="label label-default">'.$rendement_locatif_appartement.' %</span>';
							}
							//rentabilité locative egal à 3
							else if($rendement_locatif_appartement == 3){
								echo '<span class="label label-warning">'.$rendement_locatif_appartement.' %</span>';
							}
							//rentabilité locative inférieur à 3
							else if($rendement_locatif_appartement <= 2 AND $rendement_locatif_appartement >= 1){
								echo '<span class="label label-danger">'.$rendement_locatif_appartement.' %</span>';
							}
							// égal à 0 ou 0 car impossible de récupérer le m² locatif sur meilleur agent
							else if($rendement_locatif_appartement == 0){
								echo '<span class="label label-default">NC</span>';
							}
							else{
								echo '<span class="label label-default">'.$rendement_locatif_appartement.' %</span>';
							}
						}elseif($annonce['type'] == '2'){
							//rentabilité locative supérieur à 7
							if($rendement_locatif_maison > 7){
								echo '<span class="label label-success">'.$rendement_locatif_maison.' %</span> ';
							}
							//rentabilité locative entre 4 et 8
							else if($rendement_locatif_maison <= 8 AND $rendement_locatif_maison >= 4){
								echo '<span class="label label-default">'.$rendement_locatif_maison.' %</span>';
							}
							//rentabilité locative egal à 3
							else if($rendement_locatif_maison == 3){
								echo '<span class="label label-warning">'.$rendement_locatif_maison.' %</span>';
							}
							//rentabilité locative inférieur à 3
							else if($rendement_locatif_maison <= 2 AND $rendement_locatif_maison >= 1){
								echo '<span class="label label-danger">'.$rendement_locatif_maison.' %</span>';
							}
							// égal à 0 ou 0 car impossible de récupérer le m² locatif sur meilleur agent
							else if($rendement_locatif_maison == 0){
								echo '<span class="label label-default">NC</span>';
							}
							else{
								echo '<span class="label label-default">'.$rendement_locatif_maison.' %</span>';
							}
						}else{
							continue;
						}
						
						// calcul de l'assurance du crédit 
						$assurance_7ans = calcul_assurance($taux_assurance,$annonce['prix'],7);
						$assurance_10ans = calcul_assurance($taux_assurance,$annonce['prix'],10);
						$assurance_15ans = calcul_assurance($taux_assurance,$annonce['prix'],15);
						$assurance_20ans = calcul_assurance($taux_assurance,$annonce['prix'],20);
						$assurance_25ans = calcul_assurance($taux_assurance,$annonce['prix'],25);
						$assurance_30ans = calcul_assurance($taux_assurance,$annonce['prix'],30);
						
						
						// calcul mensualité
						$mensualite_7ans = round((($annonce['prix']*$taux['7ans_marche']/100/12)/(1-(1+$taux['7ans_marche']/100/12)**-(7*12)))+$assurance_7ans['mensuel']);
						$mensualite_10ans = round((($annonce['prix']*$taux['10ans_marche']/100/12)/(1-(1+$taux['10ans_marche']/100/12)**-(10*12)))+$assurance_10ans['mensuel']);
						$mensualite_15ans = round((($annonce['prix']*$taux['15ans_marche']/100/12)/(1-(1+$taux['15ans_marche']/100/12)**-(15*12)))+$assurance_15ans['mensuel']);
						$mensualite_20ans = round((($annonce['prix']*$taux['20ans_marche']/100/12)/(1-(1+$taux['20ans_marche']/100/12)**-(20*12)))+$assurance_20ans['mensuel']);
						$mensualite_25ans = round((($annonce['prix']*$taux['25ans_marche']/100/12)/(1-(1+$taux['25ans_marche']/100/12)**-(25*12)))+$assurance_25ans['mensuel']);
						$mensualite_30ans = round((($annonce['prix']*$taux['30ans_marche']/100/12)/(1-(1+$taux['30ans_marche']/100/12)**-(30*12)))+$assurance_30ans['mensuel']);
						
						
						//calcul cashflow
						//Appartement
						if($annonce['type'] == '1'){
							$cashflow_7ans = ($prix_m2_locatif_appartement*$annonce['surface']) - $mensualite_7ans;
							$cashflow_10ans = ($prix_m2_locatif_appartement*$annonce['surface']) - $mensualite_10ans;
							$cashflow_15ans = ($prix_m2_locatif_appartement*$annonce['surface']) - $mensualite_15ans;
							$cashflow_20ans = ($prix_m2_locatif_appartement*$annonce['surface']) - $mensualite_20ans;
							$cashflow_25ans = ($prix_m2_locatif_appartement*$annonce['surface']) - $mensualite_25ans;
							$cashflow_30ans = ($prix_m2_locatif_appartement*$annonce['surface']) - $mensualite_30ans;
						}
						//Maison
						elseif($annonce['type'] == '2'){
							$cashflow_7ans = ($prix_m2_locatif_maison*$annonce['surface']) - $mensualite_7ans;
							$cashflow_10ans = ($prix_m2_locatif_maison*$annonce['surface']) - $mensualite_10ans;
							$cashflow_15ans = ($prix_m2_locatif_maison*$annonce['surface']) - $mensualite_15ans;
							$cashflow_20ans = ($prix_m2_locatif_maison*$annonce['surface']) - $mensualite_20ans;
							$cashflow_25ans = ($prix_m2_locatif_maison*$annonce['surface']) - $mensualite_25ans;
							$cashflow_30ans = ($prix_m2_locatif_maison*$annonce['surface']) - $mensualite_30ans;
						}else{
							continue;
						}
						
						// si casflow positif donc supérieur à 0
						if($cashflow_20ans > 0){
							echo '<td><span class="text-success"><b>'.round($cashflow_20ans).'€/mois</b></span><br /> <small><i>Crédit : '.number_format($mensualite_20ans, 0, ',', ' ').'€/mois</i></small>';
						}
						else{
							echo '<td><span class="text-danger"><b>'.round($cashflow_20ans).'€/mois</b></span><br /> <small><i>Crédit : '. number_format($mensualite_20ans, 0, ',', ' ').'€/mois</i></small>';
						}
						
						echo '<a href="#" type="button"  data-toggle="modal" data-target="#annonce_'.$annonce['id_annonces'].'"> <span class="glyphicon glyphicon-plus"></span></a>
							
							<div class="modal fade bs-example-modal-lg" tabindex="-1" id="annonce_'.$annonce['id_annonces'].'" role="dialog" aria-labelledby="myLargeModalLabel">
							  <div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
								  <table>
									<tr>
										<td>7 ans</td>
										<td>10 ans</td>
										<td>15 ans</td>
										<td>20 ans</td>
										<td>25 ans</td>
										<td>30 ans</td>
									</tr>
									<tr>
										<td>'.$cashflow_7ans.'€</td>
										<td>'.$cashflow_10ans.'€</td>
										<td>'.$cashflow_15ans.'€</td>
										<td>'.$cashflow_20ans.'€</td>
										<td>'.$cashflow_25ans.'€</td>
										<td>'.$cashflow_30ans.'€</td>
									</tr>
								  </table>
								</div>
							  </div>
							</div>
						</td>'; 
						
						echo '</td>';
						echo '<td class="text-center">
						<a href="'.$annonce['url'].'" target="_blank" class="btn btn-primary btn-xs">Voir l\'annonce</a>
						<a href="#" onclick="save_annonce('.$annonce['id_annonces'].')" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-floppy-disk"></span></a>
						</td>';
						echo '</tr>';
					}
					ecrire_fichier_progression($filtres['url_progress'], 'terminer#'.$compteurs.'');
					//unlink(FILE_PROGRESS);
?>
				</table>
			</div>
		</div><!-- panel -->

		<div class="col-md-3">
			<div class="">
<?php 
				if($max_prix_appartement > 0 && $min_prix_appartement > 0){
?>
				<div class="panel panel-default">
					<div class="panel-heading">Analyses sur le marché immobilier de <?= $ville;?> (Appartements)</div>
					<!-- Appartement -->
					<div class="panel-body">
						<img src="https://cdn2.iconfinder.com/data/icons/font-awesome/1792/building-o-64.png" class="pull-right" />
						<ul>
							<li>Prix maximum : <?= number_format($max_prix_appartement, 0, ',', ' ');?>€</li>
							<li>Prix minimum : <?= number_format($min_prix_appartement, 0, ',', ' ');?>€</li>
							<li>Prix moyen au m² : <?= number_format($moyenne_prix_m2_appartement, 0, ',', ' ');?>€/m²
							<li>Prix au m² locatif : <?php if($prix_m2_locatif_appartement == 0){echo 'NC';}else{echo $prix_m2_locatif_appartement.'€/m²';}?></li>
						</ul>
					</div>
				</div>
<?php 
				}
?>

<?php 
				if($max_prix_maison > 0 && $min_prix_maison > 0){
?>
				<div class="panel panel-default">
					<div class="panel-heading">Analyses sur le marché immobilier de <?= $ville;?> (Maisons)</div>
					<!-- Maison -->
					<div class="panel-body">
						<img src="https://cdn3.iconfinder.com/data/icons/social-media-2125/84/house-64.png" class="pull-right" />
						<ul>
							<li>Prix maximum : <?= number_format($max_prix_maison, 0, ',', ' ');?>€</li>
							<li>Prix minimum : <?= number_format($min_prix_maison, 0, ',', ' ');?>€</li>
							<li>Prix moyen au m² : <?= number_format($moyenne_prix_m2_maison, 0, ',', ' ');?>€/m²
							<li>Prix au m² locatif : <?php if($prix_m2_locatif_maison == 0){echo 'NC';}else{echo $prix_m2_locatif_maison.'€/m²';}?></li>
						</ul>
					</div>
				</div>
<?php 
				}
?>
				<small class="text-center"><span class="glyphicon glyphicon-info-sign"></span> <i>Mise à jour annonce : <?= $date_last_scrap;?></i></small>
				<h4 class="text-center">Analyse du marché <small>(Prix, superficie, nb pièce)</small> :</h4>
				<div id="highcharts_3d" class = "graphique-content"></div>
				
				<h4 class="text-center">Répartition par type de bien : </h4>
				<div id="piechart_3d" class = "graphique-content"></div>
				
			</div>

		</div><!-- col-md-3-->

	</div><!-- row -->
<?php 
}
?>