<?php
include 'includes/fonction.php';
include 'includes/sqlConnect.php';

if(isset($_POST['login'])){
	
	$syntaxe = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
	
	//Verif Mail
	if(!preg_match($syntaxe,$_POST['email'])){
		$msg = '<div class="alert alert-danger">Le format de votre <strong>adresse email</strong> est mauvais.</div>';
	}
	else{
		// on teste si une entrée de la base contient ce couple login / pass
		$req = $pdo->query('SELECT id, count(id), prenom FROM membre WHERE email = "'.$_POST['email'].'" AND pass = "'.sha1($_POST['pass']).'"');
		$data = $req->fetch();
		$req->closeCursor();

		// si on obtient une réponse, alors l'utilisateur est un membre
		if ($data[1] == 1) {
			session_start();
			$_SESSION['email'] = $_POST['email'];
			$_SESSION['prenom'] = $data['prenom'];
			
			header('Location: search.php');//connexion normale
			exit();
		}
		// si on ne trouve aucune réponse, le visiteur s'est trompé soit dans son login, soit dans son mot de passe
		elseif ($data[0] == 0) {
			$msg = '<div class="alert alert-warning">Compte non reconnu.</div>';
		}
		// sinon, alors la, il y a un gros problème :)
		else {
			$msg = 'Probème dans la base de données.';
			//exit();
		}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Me connecter - <?= NOM_SITE;?></title>

	<?php include 'includes/meta.php';?>
</head>
<body>

    <?php include 'includes/navbar.php';?>
		 
	<div class="container">
		
		<div class="row">
		<div class="col-md-4 col-md-offset-4">
		<?php if(isset($msg)){echo $msg;}?>
		<h1>Me connecter</h1>
		<p>Trouvez des maintenant des biens avec 15% de rentabilité</p>
		<form method="POST" action="login.php">
			
			<div class="form-group">
				<label>Email</label>
				<input type="text" name="email" class="form-control" placeholder="adresse@email.com">
			</div>

			<div class="form-group">
				<label>Mot de passe</label>
				<input type="password" name="pass" class="form-control" placeholder="*****">
			</div>
			
			<div class="form-group text-right">
				<button type="submit" name="login" class="btn btn-default cta">Me connecter</button>
			</div>
		</form>
	
	</div>
	</div><!-- row -->
	

	<p class="text-center">Je n'ai pas de compte - <a href="register.php">Je m'inscris</a></p>
		
</div><!-- container -->

</body>
</html>