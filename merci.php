<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}
include 'includes/sqlConnect.php';
include 'includes/fonction.php';
require_once('stripe/config.php');

$abo = info_abonnement($_SESSION['email']);
?>
<html>
	<head>
		<title>Gestion de mon abonnement - <?= NOM_SITE;?></title>
		<?php include 'includes/meta.php';?>
		<script src="js/main.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
		<style>
		.navbar{
			margin-bottom:0px;
		}
		
		.search{
			margin-bottom:10px;
		}
		</style>
		<style>
* {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 25%;
    padding: 8px;
}

.price {
    list-style-type: none;
    border: 1px solid #eee;
    margin: 0;
    padding: 0;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.price:hover {
    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
    background-color: #111;
    color: white;
    font-size: 25px;
}

.price li {
    border-bottom: 1px solid #eee;
    padding: 20px;
    text-align: center;
}

.price .grey {
    background-color: #eee;
    font-size: 20px;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}
</style>
	</head>
	<body>
	
		<?php include 'includes/navbar.php';?>
	
		<div class="container">

		
			<div class="row">
				<div class="col-md-12">
				<h1>Merci</h1>
				<p>Merci de votre confiance et félicitation.</p>
				<p>Vous pouvez désormais utiliser nos outils.</p>
				<p class="text-center"><a href="search.php" class="btn btn-success">Faire une recherche</a></p>
				<p>Toute l'équipe d'immObserver</p>
				</div><!--/ col-md-12 -->
				
				
			</div><!-- row -->

		
		</div><!-- container -->
	</body>
</html>