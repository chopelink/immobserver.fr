<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}

include 'includes/sqlConnect.php';
include 'includes/fonction.php';
	
$action = intval($_GET['a']);

//enregistrement d'une annonce
if($action == 1){
	$id_annonce = intval($_GET['id']);
	$date = date("Y-m-d H:i:s");
	
	//insertion de l'annonce à sauvegarder
	$requete = $pdo->prepare('INSERT INTO annonces_enregistrees(email,id_annonce,date) VALUES(:email,:id_annonce,:date)');
	$requete->execute(array(
	'email' => $_SESSION['email'],
	'id_annonce' => $id_annonce,
	'date' => $date
	));
	echo 'Annonce enregistrée avec succès.';
}

//suppression d'une annonce
else if($action == 2){
	$id_annonce = intval($_GET['id']);
	$pdo->exec('DELETE FROM annonces_enregistrees WHERE email = "'.$_SESSION['email'].'" AND id_annonce = "'.$id_annonce.'"');
	$_SESSION['message'] = '<div class="alert alert-success">L\'annonce a bien été <b>supprimée</b> de votre compte.</div>';
	header ('Location: '.$_SERVER["HTTP_REFERER"].'');
	exit();
}
?>