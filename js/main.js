$( function() {
	/*function log( message ) {
	$( "<div>" ).text( message ).prependTo( "#log" );
	$( "#log" ).scrollTop( 0 );
	}*/

	$( "#ville" ).autocomplete({
		source: "autocomplete.php",
		type: "GET",
		data:'term='+$(this).val(),
		minLength: 3,

		select: function( event, ui ) {
			$( "#insee" ).val(ui.item.insee);
			$( "#postal" ).val(ui.item.postal);
			$( ".ville" ).val(ui.item.postal);
			$('input#url_progress').val(getRandomInt(1000000));
		}


		/* select: function( event, ui ) {
		$( "#insee" ).html(data);
		log( "Selected: " + ui.item.value + " aka " + ui.item.id );
		}*/
	});
	
	$(document).on('submit', 'form#FormSearch', function(){
		$('button#submit-search').prop('disabled', true);
		
		var datas = $(this).serialize();
		var myProgress = lancer_progress(datas);
		
		$("#piechart_3d").empty();
		$("#piechart_3d").html('');
		
		$("#highcharts_3d").empty();
		$("#highcharts_3d").html('');
		
		setTimeout(function(){
			$.ajax({
				type: 'GET',
				url: 'search-ajax.php',
				data: datas,
				async: true,
				success: function(resultats){
					$('div#search-content').html('');
					$('div#search-content').html(resultats);
					$('#example').DataTable( {
						"order": [[ 4, "asc" ]]
					});
					
					//Affichage du graphique
					RepartitionTypeBien(datas);
					CorrelationPrixSuperficiePiece(datas);
					
					//Temporiser l'arrêt du progressbar
					setTimeout(function(){
						clearInterval(myProgress);
						$('button#submit-search').prop('disabled', false);
					}, 150);
				}
			});
		}, 300);
		return false;
	});
});

function lancer_progress(infos){
	
	var myProgress = setInterval(function(){
		//progression('0', 'Initialisation....');
		$.ajax({
			type: 'GET',
			url: 'progress.php',
			data: infos,
			success: function(html){
				var dieze = html.indexOf("#");
				if(dieze > 0){
					var ExplodeRes = new Array;
					ExplodeRes = html.split('#');
					var Textes = $.trim(ExplodeRes[0]);
					var IntProgress = parseInt(ExplodeRes[1]);
					var Progress = 0;
					if(Textes != 'terminer' && IntProgress > 100){
						Progress = 95;
					}else if(Textes == 'terminer' && IntProgress > 0){
						Progress = 100;
					}else{
						Progress = IntProgress;
					}
					
					progression(Progress, Textes);
				}
			}
		});
	},150); 
	return myProgress;
}

function progression(indice, libelle){
	if(libelle == 'terminer' && indice > 0){
		$('div#bkg-conteneur-progression').hide();
		$('div#popup-conteneur-progression').hide();
		$('div#conteneur-progression').hide();
		$('button#submit-search').prop('disabled', false);
	}else{
		$('div#bkg-conteneur-progression').show();
		$('div#popup-conteneur-progression').show();
		$('div#conteneur-progression').show();
	}
	$('div#barre-progression').css('width', ''+indice+'%');
	$('div#pourcentage-progression').html(''+indice+'%');
	$('span#barre-progression-descr-span').html(libelle);
	$('span#barre-progression-memory-span').html('Chargement : '+$.md5(getRandomInt(1000000)));
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function RepartitionTypeBien(donnees){
	
	setTimeout(function(){
		$.ajax({
			type: 'GET',
			url: 'repartition_type_bien.php',
			data: donnees,
			success: function(datas){
				var ObjDatas = jQuery.parseJSON(datas);
				
				var Maisons = 0;
				if(ObjDatas.Maisons){
					Maisons = ObjDatas.Maisons;
				}
				
				var Appartements = 0;
				if(ObjDatas.Appartements){
					Appartements = ObjDatas.Appartements;
				}
				
				var Immeubles = 0;
				if(ObjDatas.Immeubles){
					Immeubles = ObjDatas.Immeubles;
				}
				
				var Bâtiments = 0;
				if(ObjDatas.Bâtiments){
					Bâtiments = ObjDatas.Bâtiments;
				}
				
				var Boutiques = 0;
				if(ObjDatas.Boutiques){
					Boutiques = ObjDatas.Boutiques;
				}
				
				var Autres = 0;
				if(ObjDatas.Autres){
					Autres = ObjDatas.Autres;
				}
				
				google.charts.load("current", {packages:["corechart"]});
				google.charts.setOnLoadCallback(drawChart);
				function drawChart() {
					var data = google.visualization.arrayToDataTable([
						['Task', 'Répartitions des biens'],
						['Maisons', Maisons],
						['Appartements', Appartements],
						['Immeubles', Immeubles],
						['Bâtiments', Bâtiments],
						['Boutiques', Boutiques],
						['Autres', Autres]
					]);

					var options = {
						title: 'Répartitions des biens',
						is3D: true,
						chartArea: {left:20,top:0,width:'100%',height:'auto'}, 
						legend: {position: 'bottom', alignment: 'center'},
						slices: {  
							1: {offset: 0.2},
							2: {offset: 0.3},
						},
					};

					var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
					chart.draw(data, options);
				}
			}
		});
	},150); 
}

function CorrelationPrixSuperficiePiece(donnees){
	
	setTimeout(function(){
		$.ajax({
			type: 'GET',
			url: 'correlation_prix_surface_peice.php',
			data: donnees,
			async: true,
			success: function(datas){
				var ObjDatas = {};
				var Infos = {};
				var ValLimitMax = {};
				var ValLimitMin = {};
				
				ObjDatas = jQuery.parseJSON(datas);
				Infos = ObjDatas.infos;
				ValLimitMax = ObjDatas.max;
				ValLimitMin = ObjDatas.min;
				
				var MaxPrix = 0;
				var MaxPiece = 0;
				var MaxSurface = 0;
				
				//Valeur maximum
				$.each(ValLimitMax, function(keyMax, valMax)
				{
					MaxPrix = valMax.MaxPrix;
					MaxPiece = valMax.MaxPiece;
					MaxSurface = valMax.MaxSurface;
				});
				
				var MinPrix = 0;
				var MinPiece = 0;
				var MinSurface = 0;
				
				//Valeur minimum
				$.each(ValLimitMin, function(keyMin, valMin)
				{
					MinPrix = valMin.MinPrix;
					MinPiece = valMin.MinPiece;
					MinSurface = valMin.MinSurface;
				});
				
				// Give the points a 3D feel by adding a radial gradient
				/*
				Highcharts.setOptions({});
				Highcharts.setOptions({
					colors: $.map(Highcharts.getOptions().colors, function (color) {
						return {
							radialGradient: {
								cx: 0.4,
								cy: 0.3,
								r: 0.5
							},
							stops: [
								[0, color],
								[1, Highcharts.Color(color).brighten(-0.2).get('rgb')]
							]
						};
					})
				});
				*/
				
				
				/**
				* Set up the chart
				* x = prix
				* y = Nbre piéce
				* z = superficie
				*/
				
				var datass = [];
				datass = Infos;
				var chart3D = new Highcharts.Chart({
					chart: {
						renderTo: 'highcharts_3d',
						margin: 100,
						type: 'scatter3d',
						options3d: {
							enabled: true,
							alpha: 10,
							beta: 30,
							depth: 250,
							viewDistance: 20,
							fitToPlot: false,
							frame: {
								bottom: { size: 1, color: 'rgba(0,0,0,0.02)' },
								back: { size: 1, color: 'rgba(0,0,0,0.04)' },
								side: { size: 1, color: 'rgba(0,0,0,0.06)' }
							}
						}
					},
					title: {
						text: 'Prix, supérficie et piéce'
					},
					subtitle: {
						text: 'Corrélation entre les prix d\'achat, supérficie et nombre des piéce'
					},
					plotOptions: {
						scatter: {
							width: MaxPiece,
							height: MaxPrix,
							depth: MaxSurface
						},
						series:{
							allowPointSelect: true,
							point:{
							  events:{
								select: function(e){
									var url = this.options.url;
									window.open(url);
								}
							  }
							}
						}
					},
					yAxis: {
						min: MinSurface,
						max: MaxSurface,
						title: null
					},
					xAxis: {
						min: MinPrix,
						max: MaxPrix,
						gridLineWidth: 1
					},
					zAxis: {
						min: MinPiece,
						max: MaxPiece,
						showFirstLabel: false
					},
					legend: {
						enabled: false
					},
					tooltip : {
						formatter: function() {
							var html = '';
							html += '<div>';
							html += '<b>'+this.point.options.name+'</b><br>';
							html += '<br>';
							html += '- <span>' + this.point.options.x + ' €</span><br>';
							html += '- <span>' + this.point.options.y + ' m²</span><br>';
							html += '- <span>' + this.point.options.z + ' piéce(s)</span>';
							html += '</div>';
							//console.log(this.point.options);
							return html;
						}
					},
					series: [{
						//name: 'Appartement',
						colorByPoint: true,
						data: datass
					}]
				});
				
				//console.log(datass);
				
				// Add mouse events for rotation
				$(chart3D.container).on('mousedown.hc touchstart.hc', function (eStart) {
					eStart = chart3D.pointer.normalize(eStart);

					var posX = eStart.chartX,
						posY = eStart.chartY,
						alpha = chart3D.options.chart.options3d.alpha,
						beta = chart3D.options.chart.options3d.beta,
						newAlpha,
						newBeta,
						sensitivity = 5; // lower is more sensitive

					$(document).on({
						'mousemove.hc touchmove.hc': function (e) {
							// Run beta
							e = chart3D.pointer.normalize(e);
							newBeta = beta + (posX - e.chartX) / sensitivity;
							chart3D.options.chart.options3d.beta = newBeta;

							// Run alpha
							newAlpha = alpha + (e.chartY - posY) / sensitivity;
							chart3D.options.chart.options3d.alpha = newAlpha;

							chart3D.redraw(false);
						},
						'mouseup touchend': function () {
							$(document).off('.hc');
						}
					});
				});
			}
		});
	},150); 
}