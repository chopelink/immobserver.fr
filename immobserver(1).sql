-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 17 Avril 2018 à 10:25
-- Version du serveur :  5.5.59-0+deb8u1
-- Version de PHP :  5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `immobserver`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonces_locations`
--

CREATE TABLE IF NOT EXISTS `annonces_locations` (
`id_annonces_locations` int(11) NOT NULL,
  `titre_annonces_locations` varchar(255) NOT NULL,
  `code_postal_annonces_locations` int(10) NOT NULL,
  `code_insee_annonces_locations` int(10) NOT NULL,
  `ville_annonces_locations` varchar(255) NOT NULL,
  `prix_annonces_locations` float(10,2) NOT NULL,
  `nbre_piece_annonces_locations` tinyint(5) NOT NULL,
  `nb_chambre_annonces_locations` tinyint(2) NOT NULL,
  `superficie_annonces_locations` float(10,2) NOT NULL,
  `url_annonces_locations` varchar(255) NOT NULL,
  `referrer_annonces_locations` varchar(255) NOT NULL,
  `type_annonces_locations` tinyint(2) NOT NULL,
  `date_scrap_annonces_locations` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2703 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `annonces_prix_mettre_carre`
--

CREATE TABLE IF NOT EXISTS `annonces_prix_mettre_carre` (
`id_annonces_prix_mettre_carre` int(11) NOT NULL,
  `code_postal_annonces_prix_mettre_carre` int(11) NOT NULL,
  `code_insee_annonces_prix_mettre_carre` int(11) NOT NULL,
  `valeur_annonces_prix_mettre_carre` float(10,2) NOT NULL,
  `date_annonces_prix_mettre_carre` datetime NOT NULL,
  `type_annonces_prix_mettre_carre` tinyint(2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `annonces_prix_mettre_carre_historique`
--

CREATE TABLE IF NOT EXISTS `annonces_prix_mettre_carre_historique` (
`id_annonces_prix_mettre_carre_historique` int(11) NOT NULL,
  `code_postal_annonces_prix_mettre_carre_historique` int(10) NOT NULL,
  `code_insee_annonces_prix_mettre_carre_historique` int(10) NOT NULL,
  `valeur_annonces_prix_mettre_carre_historique` float(10,2) NOT NULL,
  `date_annonces_prix_mettre_carre_historique` datetime NOT NULL,
  `type_annonces_prix_mettre_carre_historique` tinyint(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `annonces_ventes`
--

CREATE TABLE IF NOT EXISTS `annonces_ventes` (
`id_annonces_ventes` int(11) NOT NULL,
  `titre_annonces_ventes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code_postal_annonces_ventes` int(10) DEFAULT NULL,
  `code_insee_annonces_ventes` int(10) DEFAULT NULL,
  `ville_annonces_ventes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type_annonces_ventes` tinyint(2) DEFAULT NULL,
  `prix_annonces_ventes` float(10,2) DEFAULT NULL,
  `nbre_piece_annonces_ventes` tinyint(2) DEFAULT NULL,
  `nb_chambre_annonces_ventes` tinyint(2) DEFAULT NULL,
  `superficie_annonces_ventes` float(10,2) DEFAULT NULL,
  `url_annonces_ventes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `referrer_annonces_ventes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rendement_location_annonces_ventes` float(4,2) DEFAULT NULL,
  `date_scrap_annonces_ventes` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4110 DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `annonces_locations`
--
ALTER TABLE `annonces_locations`
 ADD PRIMARY KEY (`id_annonces_locations`);

--
-- Index pour la table `annonces_prix_mettre_carre`
--
ALTER TABLE `annonces_prix_mettre_carre`
 ADD PRIMARY KEY (`id_annonces_prix_mettre_carre`), ADD UNIQUE KEY `cp_ci_type` (`code_postal_annonces_prix_mettre_carre`,`code_insee_annonces_prix_mettre_carre`,`type_annonces_prix_mettre_carre`);

--
-- Index pour la table `annonces_prix_mettre_carre_historique`
--
ALTER TABLE `annonces_prix_mettre_carre_historique`
 ADD PRIMARY KEY (`id_annonces_prix_mettre_carre_historique`);

--
-- Index pour la table `annonces_ventes`
--
ALTER TABLE `annonces_ventes`
 ADD PRIMARY KEY (`id_annonces_ventes`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `annonces_locations`
--
ALTER TABLE `annonces_locations`
MODIFY `id_annonces_locations` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2703;
--
-- AUTO_INCREMENT pour la table `annonces_prix_mettre_carre`
--
ALTER TABLE `annonces_prix_mettre_carre`
MODIFY `id_annonces_prix_mettre_carre` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `annonces_prix_mettre_carre_historique`
--
ALTER TABLE `annonces_prix_mettre_carre_historique`
MODIFY `id_annonces_prix_mettre_carre_historique` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT pour la table `annonces_ventes`
--
ALTER TABLE `annonces_ventes`
MODIFY `id_annonces_ventes` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4110;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
