<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}
include 'includes/sqlConnect.php';
include 'includes/fonction.php';
?>
<html>
	<head>
		<title>Mes annonces enregistrées - <?= NOM_SITE;?></title>
		<?php include 'includes/meta.php';?>
		<script src="js/main.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
		<style>
		.corps{
			min-height:70%;
		}
		</style>
	</head>
	<body>
	
		<?php include 'includes/navbar.php';?>
	
		<div class="container">

		
			<div class="row corps">
				<div class="col-md-12">
				<?php if(isset($_SESSION['message'])){echo $_SESSION['message'];unset($_SESSION['message']);}?>
				<h1>Mes annonces sauvegardée(s) :</h1>
				<table class="table">
					<tr>
						<th>Ville</th>
						<th>Intitulé</th>
						<th>Nb pièce</th>
						<th>M²</th>
						<th>Prix</th>
						<th>Rendement<br /> locatif</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
				<?php
				$req = $pdo->query('SELECT * FROM annonces_enregistrees INNER JOIN annonces_ventes WHERE annonces_ventes.id_annonces_ventes = annonces_enregistrees.id_annonce AND annonces_enregistrees.email = "'.$_SESSION['email'].'" ORDER BY annonces_enregistrees.id DESC');
					while($data = $req->fetch()){
						echo '<tr>';
						echo '<td>'.$data['ville_annonces_ventes'].'</td>';
						echo '<td>'.$data['titre_annonces_ventes'].'</td>';
						echo '<td>'.$data['nbre_piece_annonces_ventes'].'</td>';
						echo '<td>'.$data['superficie_annonces_ventes'].'m²</td>';
						echo '<td>'.number_format($data['prix_annonces_ventes'], 0, ',', ' ').'€</td>';
						echo '<td>'.couleur_renta_locative($data['rendement_location_annonces_ventes']*100).'</td>';
						echo '<td>'.$data['date'].'</td>';
						echo '<td>
						<a href="'.$data['url_annonces_ventes'].'" class="btn btn-default" target="_blank">Voir annonce</a>
						<a href="action.php?a=2&id='.$data['id_annonce'].'" class="btn btn-danger" onclick="return(confirm(\'Etes-vous sur de vouloir supprimer cette annonce de votre compte ?\'));">Supprimer</a>
						</td>';
						echo '</tr>';
					}
				?>
				</table>
				</div><!--/ col-md-12 -->

				
			</div><!-- row -->

		
		</div><!-- container -->
		<?php include 'includes/footer.php';?>
	</body>
</html>