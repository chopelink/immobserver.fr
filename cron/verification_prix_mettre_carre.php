<?php
include dirname(__file__).'/../includes/sqlConnect.php';
include dirname(__file__).'/../includes/fonction.php';
include dirname(__file__).'/../includes/scraping.php';
include dirname(__file__).'/../includes/scraping_seloger.php';
include dirname(__file__).'/../includes/scraping_paruvendu.php';
include dirname(__file__).'/../includes/scraping_foncia.php';
include dirname(__file__).'/../includes/scraping_safti.php';
include dirname(__file__).'/../includes/arrondissement.php';

//On recupere tous les villes déjà visitées par les utilisateurs
$sqlVilles = "SELECT * FROM annonces_prix_mettre_carre WHERE DATE(date_annonces_prix_mettre_carre) < DATE(NOW()) LIMIT 1";
$sqlRecupVille = "SELECT nom_ville, code_postal, code_insee, min(code_postal) AS cp_min FROM liste_commune WHERE code_postal = :code_postal AND code_insee = :code_insee LIMIT 1";

$stmt_villes = $pdo->prepare($sqlVilles);
$stmt_villes->execute();
$donnees = array();
$donnees = $stmt_villes->fetchAll(PDO::FETCH_ASSOC);
$liste_annonce = array();
if(count($donnees) > 0){
	foreach($donnees as $annonces){
		$cp = $annonces['code_postal_annonces_prix_mettre_carre'];
		$ci = $annonces['code_insee_annonces_prix_mettre_carre'];
		
		$stmt_villes_libelle = $pdo->prepare($sqlRecupVille);
		$stmt_villes_libelle->execute(array('code_postal' => $cp, 'code_insee' => $ci));
		$ville = array();
		$ville = $stmt_villes_libelle->fetchAll(PDO::FETCH_ASSOC);
		foreach($ville as $libelle_ville){
			$city = explode ('(',$libelle_ville['nom_ville']);
			$lib_ville = trim($city[0]);
		}
	}
	
	$filtres = array();
	$filtres['ci'] = $ci;
	$filtres['cp'] = $cp;
	$filtres['ville'] = $lib_ville;
	$filtres['url_progress'] = dirname(__file__).'/../log/progress_'.$cp.'_'.date('YmdHis').'_cron.txt';
	
	$date_debut = date('Y-m-d');
	$date_fin = date('Y-m-d');
	$liste_annonce = affiche_annonces($pdo, $filtres, 0, $date_debut, $date_fin);
}
echo '<pre>';
print_r($liste_annonce);
echo '</pre>';