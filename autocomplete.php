<?php
include 'includes/sqlConnect.php';

$term = $_GET['term'];

$requete = $pdo->prepare('SELECT nom_ville, code_postal, code_insee, min(code_postal) AS cp_min FROM liste_commune WHERE nom_ville LIKE :term GROUP BY code_insee ORDER BY code_insee DESC'); // j'effectue ma requ�te SQL gr�ce au mot-cl� LIKE
$requete->execute(array('term' => ''.$term.'%'));

$array = array(); // on cr�� le tableau

while($donnee = $requete->fetch()) // on effectue une boucle pour obtenir les donn�es
{
    //array_push($array, $donnee['Nom_commune'].'|'.$donnee['Code_postal'].'|'.$donnee['Code_commune_INSEE']); // et on ajoute celles-ci � notre tableau
	array_push($array, array('value' => $donnee['nom_ville'].' ('.$donnee['cp_min'].')','insee' => $donnee['code_insee'],'postal'=>$donnee['cp_min']));
}

echo json_encode($array); // il n'y a plus qu'� convertir en JSON
?>