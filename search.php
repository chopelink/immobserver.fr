<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}

/*
http://tobiasahlin.com/blog/chartjs-charts-to-get-you-started/ (charts)

site sportif : https://www.data.gouv.fr/fr/datasets/recensement-des-equipements-sportifs-espaces-et-sites-de-pratiques/
musée https://www.data.gouv.fr/fr/datasets/liste-et-localisation-des-musees-de-france/

API :
- YellowApi (page jaune) =>http://www.yellowapi.com/

notification => http://bootstrap-notify.remabledesigns.com/
*/
include 'includes/sqlConnect.php';
include 'includes/fonction.php';
include 'includes/scraping.php';
include 'includes/scraping_seloger.php';
include 'includes/scraping_paruvendu.php';
include 'includes/scraping_foncia.php';


// information sur l'abonnement
$abo = info_abonnement($_SESSION['email']);

// information sur l'abonnement : nb_recherche, nb_audit, nb_ville, etc
$info_abo = contenu_abonnement($abo->formule);

if($abo->nb_recherche <= 0){$abonnement = false;}
//else if($abo->date_fin < date('Y-m-d H:i:s')){$abonnement = false;}
else{$abonnement = true;}
?>
<html>
	<head>
		<title>Recherche</title>
		<?php include 'includes/meta.php';?>
		<style>
		#search-content{
			min-height:60%;
		}
		</style>
		<script>
			function save_annonce(id){
				$.get( "action.php?a=1&id="+ id, function( data ) {
					$( ".reponse" ).append( '<div id="'+ id +'" class="alert alert-dismissible alert-success">' + data + '</div>');
					setTimeout(function()
					{
						document.getElementById(id).style.display = "none";
					}, 5000);
				});
			}
		</script>
	</head>
	<body style = "position:relative;">

	<?php include 'includes/navbar.php';?>

	<!-- Module de recherche -->
	<div class="search" style="background-color:#eee;padding-top:20px;">
		<div class="container">
			<form id = "FormSearch" method="GET" action="search-ajax.php">
				<div class="input-group input-group-lg">
					<span class="input-group-addon" id="sizing-addon1">Ville</span>
					<input id="ville" name="ville" placeholder="Bordeaux, Valence, etc" class="form-control">
					<span class="input-group-btn">
						<button id = "submit-search" type="submit" class="btn btn-success">GO</button>
					</span>
				</div>
				<input type="hidden" id="insee" name="code_insee">
				<input type="hidden" id="postal" name="code_postal">
				<input type="hidden" id="url_progress" name="url_progress" value = "">
			</form>
			<p class="text-center"><b><?= $abo->nb_recherche .'/'.$info_abo['nb_recherche'];?></b> recherches restantes</p>
		</div>
	</div>
	<!-- / module de recherche -->

<?php
	// affichage vers lien abonnement
	if($abonnement == false){
		echo '
		<div class="text-center" style="margin-top:0px;background-color:orange;">
		Continuez vos recherches des maintenant avec l\'offre de votre choix <a href="abonnement.php" class="btn btn-default">Je m\'abonne</a>
		</div>';
	}
?>

	<div id = "search-content" class="container-fluid"></div>
	
	<!-- Progression -->
	<div id = "bkg-conteneur-progression" style = "display:none;position: absolute; background: rgba(0,0,0,.5) none repeat scroll 0% 0%; width: 100%; height: 100%; z-index: 1000; top: 0px; left: 0px; opacity: 0.9;"></div>
	<div id = "popup-conteneur-progression" style = "display:none;position: fixed; width: 30%; background: white none repeat scroll 0% 0%; height: 200px; z-index: 2000; border: 1px solid; top: 50%; margin-top: -100px; left: 50%; margin-left: -15%;">
		<div class="text-center">En cours de recherche et d'analyse...</br>
		<img src="<?= BASE_URL;?>/images/loader.gif" /></div>
		<div id="conteneur-progression" style="background-color:transparent; position:absolute; top:100px; left:5%; margin-right:5%; height:50px; width:90%; border:1px solid #000000;">
			<div id="barre-progression" style="display:block; background-color:#CCCCCC; width:0%; height:100%;">
				<div id="pourcentage-progression" style="text-align:right; height:100%; font-size:1.8em;">
					&nbsp;
				</div>
			</div>
			<div id = "barre-progression-descr">
				<span id = "barre-progression-descr-span" style = "font-style:italic;font-size:12px;">Initialisation .....</span>
			</div>
			
			<div id = "barre-progression-descr-memory">
				<span id = "barre-progression-memory-span" style = "font-style:italic;font-size:12px;"></span>
			</div>
		</div>
	</div>
	<!-- footer -->
	<?php include 'includes/footer.php';?>
	</body>
</html>