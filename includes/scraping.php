<?php

// se loger
//paru vendu => https://www.paruvendu.fr/immobilier/annonceimmofo/liste/listeAnnonces?tt=1&tbApp=1&tbDup=1&tbChb=1&tbLof=1&tbAtl=1&tbPla=1&tbMai=1&tbVil=1&tbCha=1&tbPro=1&tbHot=1&tbMou=1&tbFer=1&at=1&nbp0=99&pa=FR&lol=0&ray=50&codeINSEE=26058,
//le bon coin =>https://www.leboncoin.fr/ventes_immobilieres/offres/rhone_alpes/occasions/?th=1&location=Marseille%2013008&ret=1&ret=2
//https://www.leboncoin.fr/ventes_immobilieres/offres/ile_de_france/occasions/?th=1&location=Mont%E9limar%2026200
//https://www.orpi.com/recherche/buy?locations%5B%5D=bourg-les-valence&sort=date-down&layoutType=mixte
//http://www.seloger.com/prix-immobilier/location/immo-le-teil-07.htm


include 'simplehtmldom/simple_html_dom.php';

function randomize_proxy(){
	
	$ListeProxy = array(
		'localhost:10020', //27911
		'localhost:10060', //27923
		'localhost:10030', //27914
		'localhost:10070', //27926
		'localhost:10040', //27917
		'localhost:10010', //27908
	);
	
	$UserAgents = array(
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
		'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
		'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
		'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1'
	);
	
	$Options = array();
	$Options['proxy'] = $ListeProxy[array_rand($ListeProxy)];
	$Options['agents'] = $UserAgents[array_rand($UserAgents)];
	return $Options;
}

function execute_recherche(array $filtres, $date_debut, $date_fin){
	global $compteurs;
	$compteurs++;
	
	require 'sqlConnect.php';

	
	//Pages à scrapper
	if(isset($filtres['pagination']) && !empty($filtres['pagination'])){
		$page = (int)$filtres['pagination'];
	}else{
		$page = 1;
	}
	
	//Recupurer le prix par mettre carré 
	if(isset($filtres['idtt']) && !empty($filtres['idtt']) && $filtres['idtt'] == 2){
		//Appartements
		$prix_m_carree_actuel_appartement = array();
		$prix_m_carree_actuel_appartement = recuperer_annonces_prix_mettre_carrer_historique($pdo, $filtres, 1, $date_debut, $date_fin);
		if(count($prix_m_carree_actuel_appartement) <= 0){
			$prix_m_carree_actuel_appartement = recuperer_annonces_prix_mettre_carrer_historique($pdo, $filtres, 2, $date_debut, $date_fin);
		}
		
		//Maisons
		$prix_m_carree_actuel_maison = array();
		$prix_m_carree_actuel_maison = recuperer_annonces_prix_mettre_carrer_historique($pdo, $filtres, 2, $date_debut, $date_fin);
		if(count($prix_m_carree_actuel_maison) <= 0){
			$prix_m_carree_actuel_maison = recuperer_annonces_prix_mettre_carrer_historique($pdo, $filtres, 1, $date_debut, $date_fin);
		}
		
		ecrire_fichier_progression($filtres['url_progress'], 'récuperation prix par mettre carrée ...#'.$compteurs.'');
	}
	
	//Scrapping seloger
	$prix_m_carre_locatif = 0;
	$prix_m_carre_locatif_appartement = 0;
	$prix_m_carre_locatif_maison = 0;
	$prix_m_carre_locatif_immeuble = 0;
	
	$loyer_total = 0;
	$loyer_total_appartement = 0;
	$loyer_total_maison = 0;
	$loyer_total_immeuble = 0;
	
	$superficie_total = 0;
	$superficie_total_appartement = 0;
	$superficie_total_maison = 0;
	$superficie_total_immeuble = 0;
	$infos = array();
	$cpt = 0;
	
	$InfosMaison = array();
	$InfosAppartement = array();
	$InfosImmeuble = array();
	for($p = 1; $p <= $page; $p++){
		$filtres['pagination'] = $p;
		$data = Collecte_data_scrapping($filtres);
		ecrire_fichier_progression($filtres['url_progress'], 'fin traitements des annonces'.$compteurs.'');
		
		//Enregistrer les annonces 
		foreach($data as $annonces){
			$compteurs++;
			if($annonces['nb_piece'] > 0 && $annonces['m_carre'] > 1 && $annonces['prix'] > 0){
				$infos[$cpt]['type'] = (isset($annonces['type']) && !empty($annonces['type'])) ? $annonces['type'] : 0;
				$infos[$cpt]['titre'] = (isset($annonces['titre']) && !empty($annonces['titre'])) ? trim(strip_tags($annonces['titre'])) : '';
				$infos[$cpt]['prix'] = (isset($annonces['prix']) && !empty($annonces['prix'])) ? trim(strip_tags($annonces['prix'])) : 0;
				$infos[$cpt]['url'] = (isset($annonces['url']) && !empty($annonces['url'])) ? trim(strip_tags($annonces['url'])) : '';
				$infos[$cpt]['nb_piece'] = (isset($annonces['nb_piece']) && !empty($annonces['nb_piece'])) ? $annonces['nb_piece'] : 0;
				$infos[$cpt]['nb_chambres'] = (isset($annonces['nb_chambres']) && !empty($annonces['nb_chambres'])) ? $annonces['nb_chambres'] : 0;
				$infos[$cpt]['m_carre'] = (isset($annonces['m_carre']) && $annonces['m_carre'] > 1) ? $annonces['m_carre'] : 0;
				$infos[$cpt]['referrer'] = (isset($annonces['referrer']) && !empty($annonces['referrer'])) ? trim(strip_tags($annonces['referrer'])) : '';
				$infos[$cpt]['cp'] = $filtres['cp'];
				$infos[$cpt]['ci'] = $filtres['ci'];
				$infos[$cpt]['ville'] = $filtres['ville'];
				$infos[$cpt]['date'] = date('Y-m-d H:i:s');
			}else{
				continue;
			}
			
			//Appartement
			if($annonces['type'] == '1'){
				$InfosAppartement[$cpt]['prix'] = $infos[$cpt]['prix'];
				$InfosAppartement[$cpt]['m_carre'] = $infos[$cpt]['m_carre'];
			}
			
			//Maison
			if($annonces['type'] == '2'){
				$InfosMaison[$cpt]['prix'] = $infos[$cpt]['prix'];
				$InfosMaison[$cpt]['m_carre'] = $infos[$cpt]['m_carre'];
			}
			
			//Rendement locatif
			if(isset($filtres['idtt']) && !empty($filtres['idtt']) && $filtres['idtt'] == 2){
				$prix = trim(strip_tags($annonces['prix']));
				if(isset($annonces['m_carre']) && !empty($annonces['m_carre'])){
					//Appartement
					if($annonces['type'] == '1'){
						$infos[$cpt]['rendement_location'] = rendement_locatif($prix_m_carree_actuel_appartement, $prix, $annonces['m_carre']);
					}
					//Maison
					elseif($annonces['type'] == '2'){
						$infos[$cpt]['rendement_location'] = rendement_locatif($prix_m_carree_actuel_maison, $prix, $annonces['m_carre']);
					}
					else{
						$infos[$cpt]['rendement_location'] = 0;
					}
				}else{
					$infos[$cpt]['rendement_location'] = 0;
				}
			}
			ecrire_fichier_progression($filtres['url_progress'], 'calcul rendement locatif ...#'.$compteurs.'');
			$cpt++;
		}
	}
	
	//Locations
	if(isset($filtres['idtt']) && !empty($filtres['idtt']) && $filtres['idtt'] == 1){
		//Appartements
		$loyer_total_appartement = $loyer_total_appartement + array_sum(array_column($InfosAppartement, 'prix'));
		$superficie_total_appartement = $superficie_total_appartement + array_sum(array_column($InfosAppartement, 'm_carre'));
		
		//Maisons
		$loyer_total_maison = $loyer_total_maison + array_sum(array_column($InfosMaison, 'prix'));
		$superficie_total_maison = $superficie_total_maison + array_sum(array_column($InfosMaison, 'm_carre'));
	}
	
	//Enregistrer les annonces
	if($filtres['idtt'] == 1){
		enregistrer_annonces($pdo, $infos, 'annonces_locations');
	}elseif($filtres['idtt'] == 2){
		enregistrer_annonces($pdo, $infos, 'annonces_ventes');
	}
	
	//Prix au mettre carré appartement
	if($superficie_total_appartement > 0){
		$infos_prix_m_carre_locatif_appartement = array();
		$prix_m_carre_locatif_appartement = round($loyer_total_appartement/$superficie_total_appartement, 2);
		$infos_prix_m_carre_locatif_appartement['prix_m_carre_locatif'] = $prix_m_carre_locatif_appartement;
		$infos_prix_m_carre_locatif_appartement['loyer_total'] = $loyer_total_appartement;
		$infos_prix_m_carre_locatif_appartement['superficie_total'] = $superficie_total_appartement;
		$infos_prix_m_carre_locatif_appartement['code_postal'] = $filtres['cp'];
		$infos_prix_m_carre_locatif_appartement['code_insee'] = $filtres['ci'];
		$infos_prix_m_carre_locatif_appartement['date'] = date('Y-m-d H:i:s');
		enregistrer_annonces_prix_mettre_carrer($pdo, $infos_prix_m_carre_locatif_appartement, 1);
		enregistrer_annonces_prix_mettre_carrer_historique($pdo, $infos_prix_m_carre_locatif_appartement, 1);
	}
	
	//Prix au mettre carré maison
	if($superficie_total_maison > 0){
		$infos_prix_m_carre_locatif_maison = array();
		$prix_m_carre_locatif_maison = round($loyer_total_maison/$superficie_total_maison, 2);
		$infos_prix_m_carre_locatif_maison['prix_m_carre_locatif'] = $prix_m_carre_locatif_maison;
		$infos_prix_m_carre_locatif_maison['loyer_total'] = $loyer_total_maison;
		$infos_prix_m_carre_locatif_maison['superficie_total'] = $superficie_total_maison;
		$infos_prix_m_carre_locatif_maison['code_postal'] = $filtres['cp'];
		$infos_prix_m_carre_locatif_maison['code_insee'] = $filtres['ci'];
		$infos_prix_m_carre_locatif_maison['date'] = date('Y-m-d H:i:s');
		enregistrer_annonces_prix_mettre_carrer($pdo, $infos_prix_m_carre_locatif_maison, 2);
		enregistrer_annonces_prix_mettre_carrer_historique($pdo, $infos_prix_m_carre_locatif_maison, 2);
	}

	return $infos;
}

function Collecte_data_scrapping(array $filtres){
	global $compteurs;
	$ListeSites = array('seloger', 'paruvendu', 'foncia', 'safti', 'orpi');
	$datas = array();
	foreach($ListeSites as $Sites){
		$compteurs++;
		if($Sites == 'foncia'){
			$data = scrapping_foncia_recurssive($filtres);
		}else{
			$SiteFonctionScrap = 'scrapping_'.$Sites.'_new';
			$html = $SiteFonctionScrap($filtres);
			
			$SiteFonctionDatas = 'traiter_data_'.$Sites.'';
			$data = $SiteFonctionDatas($html);
		}
		
		foreach($data as $infosElement){
			ecrire_fichier_progression($filtres['url_progress'], 'traitements des annonces ...#'.$compteurs.'');
			$datas[] = $infosElement;
		}
	}
	return $datas;
}

function verif_annonces($pdo, array $params){
	//Date moin de 6 heures
	$dateVerif = date('Y-m-d H:i:s',mktime(date('H')-6,date('i'),date('s'),(int)date('n'),(int)date('d'),date('Y')));
	$sqlVerif = "SELECT 
			COUNT(*)
			FROM annonces_ventes
			WHERE code_insee_annonces_ventes = '".$params['ci']."'
			AND code_postal_annonces_ventes = '".$params['cp']."'
			AND date_scrap_annonces_ventes > '".$dateVerif."'
			AND LOWER(titre_annonces_ventes) NOT LIKE '%immeub%'
	";
	$req = $pdo->query($sqlVerif);
	if($req->fetchColumn() > 0){
		return false;
	}else{
		return true;
	}
}

function affiche_annonces($pdo, array $filtres, $tentative, $date_debut, $date_fin){
	global $compteurs;
	
	if($tentative == 0){
		ecrire_fichier_progression($filtres['url_progress'], 'vérification dans la base ...#'.$compteurs.'');
	}
	
	$Params = array();
	$Params['ci'] = $filtres['ci'];
	$Params['cp'] = $filtres['cp'];
	$Params['idtypebien'] = '2,1';//Appartement et maison
	$Params['naturebien'] = '1,2,4';
	$Params['pagination'] = 2;
	$Params['ville'] = $filtres['ville'];
	$Params['url_progress'] = $filtres['url_progress'];
	
	$sql_ventes = "SELECT 
				id_annonces_ventes as id_annonces,
				titre_annonces_ventes as titre,
				code_postal_annonces_ventes as code_postal,
				code_insee_annonces_ventes as code_insee,
				ville_annonces_ventes as ville,
				prix_annonces_ventes as prix,
				nbre_piece_annonces_ventes as nbre_piece,
				nb_chambre_annonces_ventes as nbre_chambre,
				superficie_annonces_ventes as surface,
				url_annonces_ventes as url,
				referrer_annonces_ventes as referrer,
				type_annonces_ventes as type,
				MAX(date_scrap_annonces_ventes) as date,
				AVG(rendement_location_annonces_ventes) as rendement
				FROM annonces_ventes
				WHERE code_insee_annonces_ventes = '".$filtres['ci']."'
				AND code_postal_annonces_ventes = '".$filtres['cp']."'
				AND (DATE(date_scrap_annonces_ventes) BETWEEN '".$date_debut."' AND '".$date_fin."')
				AND LOWER(titre_annonces_ventes) NOT LIKE '%immeub%'
				GROUP BY url_annonces_ventes
				ORDER BY date_scrap_annonces_ventes DESC
			";
	
	$req = $pdo->prepare($sql_ventes);
	$req->execute();
	$donnees = $req->fetchAll(PDO::FETCH_ASSOC);
	$req->closeCursor();
	
	$liste_annonce = array();
	$liste_annonce = $donnees;
	
	if(count($liste_annonce) == 0 && $tentative == 0){
		ecrire_fichier_progression($filtres['url_progress'], 'recherche en cours ...#'.$compteurs.'');
		
		$Params['idtt'] = '1';//Location
		$dataLocations = execute_recherche($Params, $date_debut, $date_fin);

		$Params['idtt'] = '2';//Ventes
		$dataVentes = execute_recherche($Params, $date_debut, $date_fin);
		
		$tentative++;
		$liste_annonce = affiche_annonces($pdo, $filtres, $tentative, $date_debut, $date_fin);
	}elseif(count($liste_annonce) > 0){
		//Verification si on va scrapper ou pas
		$IsScrapp = verif_annonces($pdo, $Params);
		if($IsScrapp){
			ecrire_fichier_progression($filtres['url_progress'], 'recherche en cours ...#'.$compteurs.'');
		
			$Params['idtt'] = '1';//Location
			$dataLocations = execute_recherche($Params, $date_debut, $date_fin);

			$Params['idtt'] = '2';//Ventes
			$dataVentes = execute_recherche($Params, $date_debut, $date_fin);
			
			$tentative++;
			$liste_annonce = affiche_annonces($pdo, $filtres, $tentative, $date_debut, $date_fin);
		}
	}
	return $liste_annonce;
}

function rendement_locatif(array $prix_m2_locatif, $prix_vente, $superficie_total){
	//((prix m² locatif x superficie)x12) /prix
	$prix_m2_locatif_moyenne = 0;
	if(count($prix_m2_locatif) > 0){
		//Moyenne des prix au mettre carrée
		$cpt = 0;
		$somme_prix_m2_locatif = 0;
		foreach($prix_m2_locatif as $m2_locatif){
			$somme_prix_m2_locatif = $somme_prix_m2_locatif + $m2_locatif['valeur_annonces_prix_mettre_carre_historique'];
			$cpt++;
		}
		$prix_m2_locatif_moyenne = round($somme_prix_m2_locatif/$cpt, 2);
	}
	
	if($prix_vente > 0){
		$rendement = round((($prix_m2_locatif_moyenne * $superficie_total)*12)/$prix_vente, 2);
	}else{
		$rendement = 0;
	}
	return $rendement;
}

function recuperer_annonces_prix_mettre_carrer($pdo, array $filtres, $type){
	$sql = "SELECT 
		valeur_annonces_prix_mettre_carre 
		FROM annonces_prix_mettre_carre 
		WHERE code_insee_annonces_prix_mettre_carre = '".$filtres['ci']."' 
		AND code_postal_annonces_prix_mettre_carre = '".$filtres['cp']."'
		AND type_annonces_prix_mettre_carre = '".$type."'
		LIMIT 1
	";
	$stmt = $pdo->query($sql);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	return $row['valeur_annonces_prix_mettre_carre'];
}

function recuperer_annonces_prix_mettre_carrer_historique($pdo, array $filtres, $type, $date_debut, $date_fin){
	$sql = "SELECT 
		*
		FROM annonces_prix_mettre_carre_historique 
		WHERE code_insee_annonces_prix_mettre_carre_historique = '".$filtres['ci']."' 
		AND code_postal_annonces_prix_mettre_carre_historique = '".$filtres['cp']."'
		AND type_annonces_prix_mettre_carre_historique = '".$type."'
		AND (DATE(date_annonces_prix_mettre_carre_historique) BETWEEN '".$date_debut."' AND '".$date_fin."')
	";
	$stmt = $pdo->query($sql);
	$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $row;
}

function enregistrer_annonces_prix_mettre_carrer($pdo, $data, $type){
	$stmt_annonces_prix_mettre_carrer = $pdo->prepare('
		INSERT INTO annonces_prix_mettre_carre (
			code_postal_annonces_prix_mettre_carre, 
			code_insee_annonces_prix_mettre_carre, 
			valeur_annonces_prix_mettre_carre, 
			date_annonces_prix_mettre_carre,
			type_annonces_prix_mettre_carre
		) 
		VALUES(
			:code_postal_annonces_prix_mettre_carre, 
			:code_insee_annonces_prix_mettre_carre, 
			:valeur_annonces_prix_mettre_carre, 
			:date_annonces_prix_mettre_carre,
			:type_annonces_prix_mettre_carre
		)
		ON DUPLICATE KEY UPDATE
		    valeur_annonces_prix_mettre_carre = :valeur_annonces_prix_mettre_carre,
			date_annonces_prix_mettre_carre = :date_annonces_prix_mettre_carre
	');

	$stmt_annonces_prix_mettre_carrer->bindParam(':code_postal_annonces_prix_mettre_carre', $data['code_postal']);
	$stmt_annonces_prix_mettre_carrer->bindParam(':code_insee_annonces_prix_mettre_carre', $data['code_insee']);
	$stmt_annonces_prix_mettre_carrer->bindParam(':valeur_annonces_prix_mettre_carre', $data['prix_m_carre_locatif']);
	$stmt_annonces_prix_mettre_carrer->bindParam(':date_annonces_prix_mettre_carre', $data['date']);
	$stmt_annonces_prix_mettre_carrer->bindParam(':type_annonces_prix_mettre_carre', $type);
	$stmt_annonces_prix_mettre_carrer->execute();
}

function enregistrer_annonces_prix_mettre_carrer_historique($pdo, $data, $type){
	$stmt_annonces_prix_mettre_carre_historique = $pdo->prepare('
		INSERT INTO annonces_prix_mettre_carre_historique (
			code_postal_annonces_prix_mettre_carre_historique, 
			code_insee_annonces_prix_mettre_carre_historique, 
			valeur_annonces_prix_mettre_carre_historique, 
			date_annonces_prix_mettre_carre_historique,
			type_annonces_prix_mettre_carre_historique
		) 
		VALUES(
			:code_postal_annonces_prix_mettre_carre_historique, 
			:code_insee_annonces_prix_mettre_carre_historique, 
			:valeur_annonces_prix_mettre_carre_historique, 
			:date_annonces_prix_mettre_carre_historique,
			:type_annonces_prix_mettre_carre_historique
		)
	');

	$stmt_annonces_prix_mettre_carre_historique->bindParam(':code_postal_annonces_prix_mettre_carre_historique', $data['code_postal']);
	$stmt_annonces_prix_mettre_carre_historique->bindParam(':code_insee_annonces_prix_mettre_carre_historique', $data['code_insee']);
	$stmt_annonces_prix_mettre_carre_historique->bindParam(':valeur_annonces_prix_mettre_carre_historique', $data['prix_m_carre_locatif']);
	$stmt_annonces_prix_mettre_carre_historique->bindParam(':date_annonces_prix_mettre_carre_historique', $data['date']);
	$stmt_annonces_prix_mettre_carre_historique->bindParam(':type_annonces_prix_mettre_carre_historique', $type);
	$stmt_annonces_prix_mettre_carre_historique->execute();
}

function enregistrer_annonces($pdo, $data, $table){
	
	$ChampInsert = '';
	$ChampValues = '';
	$ChampOnDuplicate = '';
	
	if($table == 'annonces_ventes'){
		$ChampInsert .= 'rendement_location_annonces_ventes,';
		$ChampValues .= ':rendement_location_annonces_ventes,';
	}
	
	$ChampInsert .= '
		titre_'.$table.', 
		code_postal_'.$table.', 
		code_insee_'.$table.', 
		ville_'.$table.', 
		prix_'.$table.', 
		nbre_piece_'.$table.', 
		nb_chambre_'.$table.', 
		superficie_'.$table.', 
		url_'.$table.', 
		referrer_'.$table.', 
		type_'.$table.', 
		date_scrap_'.$table.'
	';
	
	
	$ChampValues .= '
		:titre_'.$table.', 
		:code_postal_'.$table.', 
		:code_insee_'.$table.', 
		:ville_'.$table.', 
		:prix_'.$table.', 
		:nbre_piece_'.$table.', 
		:nb_chambre_'.$table.', 
		:superficie_'.$table.', 
		:url_'.$table.', 
		:referrer_'.$table.', 
		:type_'.$table.', 
		:date_scrap_'.$table.'
	';
	
	$stmt_annonces = $pdo->prepare('INSERT INTO '.$table.' ('.$ChampInsert.') VALUES('.$ChampValues.') ');

	foreach($data as $datas){
		if($table == 'annonces_ventes'){
			$stmt_annonces->bindParam(':rendement_location_annonces_ventes', $datas['rendement_location']);
		}
		
		$stmt_annonces->bindParam(':titre_'.$table.'', $datas['titre']);
		$stmt_annonces->bindParam(':code_postal_'.$table.'', $datas['cp']);
		$stmt_annonces->bindParam(':code_insee_'.$table.'', $datas['ci']);
		$stmt_annonces->bindParam(':ville_'.$table.'', $datas['ville']);
		$stmt_annonces->bindParam(':prix_'.$table.'', $datas['prix']);
		$stmt_annonces->bindParam(':nbre_piece_'.$table.'', $datas['nb_piece']);
		$stmt_annonces->bindParam(':nb_chambre_'.$table.'', $datas['nb_chambres']);
		$stmt_annonces->bindParam(':superficie_'.$table.'', $datas['m_carre']);
		$stmt_annonces->bindParam(':url_'.$table.'', $datas['url']);
		$stmt_annonces->bindParam(':referrer_'.$table.'', $datas['referrer']);
		$stmt_annonces->bindParam(':type_'.$table.'', $datas['type']);
		$stmt_annonces->bindParam(':date_scrap_'.$table.'', $datas['date']);
		$stmt_annonces->execute();
	}
}

function lancer_scrapp_taux_immobilier($pdo){
	$data = scrapping_taux_new();
	$datas = traiter_data_taux($data);
	$structure = array();
	foreach($datas as $taux){
		//7 ans
		if($taux['annee'] == 7){
			$structure['7ans_min'] = $taux['taux_min'];
			$structure['7ans_marche'] = $taux['taux_marcher'];
			$structure['7ans_max'] = $taux['taux_max'];
		}
		
		//10 ans
		if($taux['annee'] == 10){
			$structure['10ans_min'] = $taux['taux_min'];
			$structure['10ans_marche'] = $taux['taux_marcher'];
			$structure['10ans_max'] = $taux['taux_max'];
		}
		
		//15 ans
		if($taux['annee'] == 15){
			$structure['15ans_min'] = $taux['taux_min'];
			$structure['15ans_marche'] = $taux['taux_marcher'];
			$structure['15ans_max'] = $taux['taux_max'];
		}
		
		//20 ans
		if($taux['annee'] == 20){
			$structure['20ans_min'] = $taux['taux_min'];
			$structure['20ans_marche'] = $taux['taux_marcher'];
			$structure['20ans_max'] = $taux['taux_max'];
		}
		
		//25 ans
		if($taux['annee'] == 25){
			$structure['25ans_min'] = $taux['taux_min'];
			$structure['25ans_marche'] = $taux['taux_marcher'];
			$structure['25ans_max'] = $taux['taux_max'];
		}
		
		//30 ans
		if($taux['annee'] == 30){
			$structure['30ans_min'] = $taux['taux_min'];
			$structure['30ans_marche'] = $taux['taux_marcher'];
			$structure['30ans_max'] = $taux['taux_max'];
		}
	}
	enregistrer_taux_immobilier($pdo, $structure);
	return $structure;
}

function enregistrer_taux_immobilier($pdo, $datas){
	/*
	$stmt_taux_immobilier_truncate = $pdo->prepare('TRUNCATE TABLE taux_immobilier');
	$stmt_taux_immobilier_truncate->execute();
	*/
	
	$stmt_taux_immobilier = $pdo->prepare('
		INSERT INTO taux_immobilier (
			date, 
			7ans_min, 
			7ans_marche, 
			7ans_max,
			10ans_min,
			10ans_marche,
			10ans_max,
			15ans_min,
			15ans_marche,
			15ans_max,
			20ans_min,
			20ans_marche,
			20ans_max,
			25ans_min,
			25ans_marche,
			25ans_max,
			30ans_min,
			30ans_marche,
			30ans_max
		) 
		VALUES(
			:date, 
			:7ans_min, 
			:7ans_marche, 
			:7ans_max,
			:10ans_min,
			:10ans_marche,
			:10ans_max,
			:15ans_min,
			:15ans_marche,
			:15ans_max,
			:20ans_min,
			:20ans_marche,
			:20ans_max,
			:25ans_min,
			:25ans_marche,
			:25ans_max,
			:30ans_min,
			:30ans_marche,
			:30ans_max
		)
		ON DUPLICATE KEY UPDATE
			7ans_min = :7ans_min, 
			7ans_marche = :7ans_marche, 
			7ans_max = :7ans_max,
			10ans_min = :10ans_min,
			10ans_marche = :10ans_marche,
			10ans_max = :10ans_max,
			15ans_min = :15ans_min,
			15ans_marche = :15ans_marche,
			15ans_max = :15ans_max,
			20ans_min = :20ans_min,
			20ans_marche = :20ans_marche,
			20ans_max = :20ans_max,
			25ans_min = :25ans_min,
			25ans_marche = :25ans_marche,
			25ans_max = :25ans_max,
			30ans_min = :30ans_min,
			30ans_marche = :30ans_marche,
			30ans_max = :30ans_max
	');
	
	$date = date('Y-m-d');
	$stmt_taux_immobilier->bindParam(':date', $date);
	
	$stmt_taux_immobilier->bindParam(':7ans_min', $datas['7ans_min']);
	$stmt_taux_immobilier->bindParam(':7ans_marche', $datas['7ans_marche']);
	$stmt_taux_immobilier->bindParam(':7ans_max', $datas['7ans_max']);
	
	$stmt_taux_immobilier->bindParam(':10ans_min', $datas['10ans_min']);
	$stmt_taux_immobilier->bindParam(':10ans_marche', $datas['10ans_marche']);
	$stmt_taux_immobilier->bindParam(':10ans_max', $datas['10ans_max']);
	
	$stmt_taux_immobilier->bindParam(':15ans_min', $datas['15ans_min']);
	$stmt_taux_immobilier->bindParam(':15ans_marche', $datas['15ans_marche']);
	$stmt_taux_immobilier->bindParam(':15ans_max', $datas['15ans_max']);
	
	$stmt_taux_immobilier->bindParam(':20ans_min', $datas['20ans_min']);
	$stmt_taux_immobilier->bindParam(':20ans_marche', $datas['20ans_marche']);
	$stmt_taux_immobilier->bindParam(':20ans_max', $datas['20ans_max']);
	
	$stmt_taux_immobilier->bindParam(':25ans_min', $datas['25ans_min']);
	$stmt_taux_immobilier->bindParam(':25ans_marche', $datas['25ans_marche']);
	$stmt_taux_immobilier->bindParam(':25ans_max', $datas['25ans_max']);
	
	$stmt_taux_immobilier->bindParam(':30ans_min', $datas['30ans_min']);
	$stmt_taux_immobilier->bindParam(':30ans_marche', $datas['30ans_marche']);
	$stmt_taux_immobilier->bindParam(':30ans_max', $datas['30ans_max']);
	
	$stmt_taux_immobilier->execute();
}

//se loger
function scraping_seloger($insee,$ville,$code_postal,$rendement_locatif){
	
	/* info_m2
	0 = appartement
	1 = maison
	*/
	$prix_m2_locatif = $rendement_locatif;
	
	print_r($prix_m2_locatif);

	//création de l'url avec le code insee
	$code_insee = substr($insee,0,2).'0'.substr($insee,2,5);
	$url = 'http://www.seloger.com/list.htm?tri=initial&idtypebien=2,1&idtt=2&ci='.$code_insee.'&naturebien=1,2,4';
	echo '<p>seloger url = '.$url.'</p>';
	
	$curl = curl_init(); 

	curl_setopt($curl, CURLOPT_URL, $url);  
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);  
	$str = curl_exec($curl);  
	curl_close($curl);  

	$html = str_get_html($str); 
	//$html = file_get_html($url[0]);

	$i = 0;
	$annonce = array();
	foreach($html->find('.c-pa-list') as $element){
		/* ancienne scrap => seloger à changer les balises
		//titre
		$annonce[$i]['titre'] = trim($element->find('.title a', 0)->plaintext);
		//prix
		$annonce[$i]['prix'] = traitement_prix(trim($element->find('.price a',0)->plaintext));
		//url
		$annonce[$i]['url'] = $element->find('.title a', 0)->href;
		//mettre carré
		$annonce[$i]['m_carre'] = $element->find('ul[class=property_list] li', 2)->plaintext;
		$tab_m2 = explode(' ',$annonce[$i]['m_carre']);
		$annonce[$i]['m_carre'] = $tab_m2[0];
		*/
		
		//type : maison,appartement
		$annonce[$i]['type'] = '';
		
		//titre
		$annonce[$i]['titre'] = trim($element->find('.c-pa-link', 0)->plaintext);

		//prix
		$annonce[$i]['prix'] = traitement_prix(trim($element->find('.c-pa-cprice',0)->plaintext));

		//url
		$annonce[$i]['url'] = $element->find('.c-pa-link', 0)->href;
		
		//type : appartement ou maison
		//maison
		if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
			$annonce[$i]['type'] = 2;
		}
		else if(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
			$annonce[$i]['type'] = 1;
		}
		else if(preg_match('#immeuble#i',$annonce[$i]['titre'])){
			$annonce[$i]['type'] = 4;
		}
		else{
			$annonce[$i]['type'] = 0;
		}
		
		
		foreach($element->find('.c-pa-criterion em') as $elmt){
			$tab_elmt = explode(' ',$elmt);

			if(preg_match('#p#',$elmt)){
				$annonce[$i]['nb_piece'] = trim($tab_elmt[0]);
				//echo $elmt.' [piece] '.$tab_elmt[0].'<br />';
			}
			else if(preg_match('#m²#',$elmt)){
				$annonce[$i]['m_carre'] = trim($tab_elmt[0]);
				//echo $elmt.' [metre carré] '.$tab_elmt[0].'<br />';
			}
			else{
				//echo $elmt.' <br />';
				//echo $elmt.' 0 pie<br />';
				//$annonce[$i]['nb_piece'] = 0;
				//$annonce[$i]['m_carre'] = 0;
			}
		}
		
		// si pas de m² ou nb_piece dans l'annonce scrappée
		if(!isset($annonce[$i]['nb_piece'])){$annonce[$i]['nb_piece'] = 0;}
		if(!isset($annonce[$i]['m_carre'])){$annonce[$i]['m_carre'] = 0;}
		
		/*
		if(preg_match('#p#',$element->find('.c-pa-criterion em', 0)->plaintext)){
			$annonce[$i]['nb_piece'] = traitement_prix($element->find('.c-pa-criterion em', 0)->plaintext);
		}
		else if(preg_match('#p#',$element->find('.c-pa-criterion em', 1)->plaintext)){
			$annonce[$i]['nb_piece'] = traitement_prix($element->find('.c-pa-criterion em', 1)->plaintext);
		}
		else{
			$annonce[$i]['nb_piece'] = 0;
		}
		
		//m2 carré
		if(preg_match('#m²#',$element->find('.c-pa-criterion em', 1)->plaintext)){
			$annonce[$i]['m_carre'] = $element->find('.c-pa-criterion em', 1)->plaintext;
			$tab_m2 = explode(' ',$annonce[$i]['m_carre']);
			$annonce[$i]['m_carre'] = $tab_m2[0];
		}
		else if(preg_match('#m²#',$element->find('.c-pa-criterion em', 2)->plaintext)){
			$annonce[$i]['m_carre'] = $element->find('.c-pa-criterion em', 2)->plaintext;
			$tab_m2 = explode(' ',$annonce[$i]['m_carre']);
			$annonce[$i]['m_carre'] = $tab_m2[0];
		}
		else{
			$annonce[$i]['m_carre'] = 0;
		}*/
		
		// rendement locatif
		if($annonce[$i]['type'] == 1){//appartement
			echo '<p>Prix m2 locatif appartement = '.$prix_m2_locatif[0].'</p>';
			echo '<p>m2 apprtment = '.$annonce[$i]['m_carre'].'</p>';
			echo '<p>prix apprtment = '.$annonce[$i]['prix'].'</p>';
			$annonce[$i]['rendement_locatif'] = round((($annonce[$i]['m_carre']*$prix_m2_locatif[0])*12)/$annonce[$i]['prix'],2)*100;
			echo '<p>rendement = '.$annonce[$i]['rendement_locatif'].'</p>';
		}
		else{// maison et autre (immeuble, etc)
			echo '<p>Prix m2 locatif = '.$prix_m2_locatif[1].'</p>';
			echo '<p>m2 = '.$annonce[$i]['m_carre'].'</p>';
			echo '<p>prix = '.$annonce[$i]['prix'].'</p>';
			echo gettype($annonce[$i]['m_carre']);
			echo $annonce[$i]['m_carre']*$prix_m2_locatif[1];
			$annonce[$i]['rendement_locatif'] = round((($annonce[$i]['m_carre']*$prix_m2_locatif[1])*12)/$annonce[$i]['prix'],2)*100;
			echo '<p>'.$annonce[$i]['rendement_locatif'].'</p>';
		}
		
		$annonce[$i]['source'] = 'SeLoger';
		
		$date = date("Y-m-d H:i:s");
		
		$i++;
	}
	
	/* Affichage résultat */
	//Affichage des annonces

	echo '<pre>';
	print_r($annonce);
	echo '</pre>';
	
	return $annonce;
}


//scrapping prix m² + prix m² locatif
function scrapping_meilleursagents($nom_ville,$code_postal){
	
	$ville = strtolower(str_replace(' ','-',$nom_ville));
	$cp = "$code_postal";
	
	/* Gestion des villes avec arrondissement */
	include 'arrondissement.php';
	if(($code_postal >= 75000 && $code_postal <= 75020) OR ($code_postal >= 69001 && $code_postal <= 69009) OR ($code_postal >= 13001 && $code_postal <= 13016)){
		$url ='https://www.meilleursagents.com/prix-immobilier/'.$arrondissement[$cp]['meilleuragent'].'-'.$cp.'/';
	}
	else{
		$url ='https://www.meilleursagents.com/prix-immobilier/'.$ville.'-'.$cp.'/';
	}

	$curl = curl_init(); 

	curl_setopt($curl, CURLOPT_URL, $url);  
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);  
	$str = curl_exec($curl);  
	curl_close($curl);  

	$html = str_get_html($str); 
	$info_m2 = array();
	foreach($html->find('.big-number') as $element){
		$info_m2[] = traitement_prix_virgule(trim($element->plaintext));
	}
	echo '<p>Meilleur agent : '.$url.'</p>';
	return $info_m2;

	
	/*
	tableau :
	[0] = prix m2 appartement
	[1] = prix m2 maison
	[2] = prix m2 locatif
	[3] = evolution du prix de l'immobilier dans le département
	*/
	
	/*echo '<pre>';
	print_r($info_m2);
	echo '</pre>';*/
}

/* PARU VENDU */
function scrapping_paruvendu($insee,$ville,$code_postal,$rendement_locatif){
	
	/* info_m2
	0 = appartement
	1 = maison
	*/
	$prix_m2_locatif = $rendement_locatif;
	
	$url ='https://www.paruvendu.fr/immobilier/annonceimmofo/liste/listeAnnonces?tt=1&tbApp=1&tbDup=1&tbChb=1&tbLof=1&tbAtl=1&tbPla=1&tbMai=1&tbVil=1&tbCha=1&tbPro=1&tbHot=1&tbMou=1&tbFer=1&at=1&nbp0=99&pa=FR&lol=0&ray=50&codeINSEE='.$insee.',';

	$curl = curl_init(); 

	curl_setopt($curl, CURLOPT_URL, $url);  
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);  
	$str = curl_exec($curl);  
	curl_close($curl);  

	$html = str_get_html($str); 
	//$html = file_get_html($url[0]);

	$i = 0;
	$annonce = array();
	foreach($html->find('.ergov3-annonce') as $element){
		
		$tab = explode(',',trim($element->find('h3', 0)->plaintext));
		
		print_r($tab);
		
		if(!isset($tab[1])){//c'estune publicité et pas une annonce
		}
		else{
			$tab_m2 = explode(' ',$tab[1]);
			
			//type : maison,appartement
			$annonce[$i]['type'] = '';
			
			//titre
			$annonce[$i]['titre'] = $tab[0];
			$annonce[$i]['prix'] = traitement_prix(trim($element->find('.ergov3-priceannonce', 0)->plaintext));
			
			//url
			$annonce[$i]['url'] = 'https://www.paruvendu.fr'.$element->find('a', 1)->href;
			
			//m2
			$annonce[$i]['m_carre'] = trim($tab_m2[0]);
			
			//nb pièce
			$annonce[$i]['nb_piece'] = traitement_prix($tab[0]);
	
			
			$annonce[$i]['source'] = 'ParuVendu';
			
			//type : appartement ou maison
			if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 2;
			}
			else if(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 1;
			}
			else if(preg_match('#immeuble#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 4;
			}
			else{
				$annonce[$i]['type'] = 0;
			}
			
			
			// rendement locatif
			if($annonce[$i]['type'] == 1){//appartement
				$annonce[$i]['rendement_locatif'] = round((($annonce[$i]['m_carre']*$prix_m2_locatif[0])*12)/$annonce[$i]['prix'],2)*100;
			}
			else{// maison et autre (immeuble, etc)
				$annonce[$i]['rendement_locatif'] = round((($annonce[$i]['m_carre']*$prix_m2_locatif[1])*12)/$annonce[$i]['prix'],2)*100;
			}
			
			$date = date("Y-m-d H:i:s");
		}
		
		$i++;
	}
	
	echo '<p>paruVendu url = '.$url.'</p>';
	/*echo '<pre>';
	print_r($annonce);
	echo '</pre>';*/
	
	return $annonce;
}


//vivastreet
function scraping_vivastreet($insee,$ville,$code_postal,$rendement_locatif){
	
	/* info_m2
	0 = appartement
	1 = maison
	*/
	$prix_m2_locatif = $rendement_locatif;
	
	$ville = str_replace(' ','-',$ville);
	
	/* Gestion des villes avec arrondissement */
	include 'arrondissement.php';
	if(($code_postal >= 75000 && $code_postal <= 75020) OR ($code_postal >= 69001 && $code_postal <= 69009) OR ($code_postal >= 13001 && $code_postal <= 13016)){
		$url = 'http://www.vivastreet.com/immobilier-vente-appartement/'.$arrondissement[$code_postal]['vivastreet'].'-'.$code_postal.'';
	}
	else{
		$url = 'http://www.vivastreet.com/immobilier-vente-appartement/'.strtolower($ville).'-'.$code_postal.'';
	}

	//création de l'url avec le code insee
	//$url = 'http://www.vivastreet.com/immobilier-vente-appartement/'.strtolower($ville).'-'.$code_postal.'';
	
	$curl = curl_init(); 

	curl_setopt($curl, CURLOPT_URL, $url);  
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);  
	$str = curl_exec($curl);  
	curl_close($curl);  

	$html = str_get_html($str); 

	$i = 0;
	$annonce = array();//evite de bugger l'ensemble du scrapp si cela plante => autre site
	foreach($html->find('.classified') as $element){
		
		$verif = trim($element->find('.lnk', 0)->plaintext);
		
		if(preg_match('#'.$code_postal.'#',$verif) OR preg_match('#Lyon#',$verif) OR preg_match('#Marseille#',$verif)){
			
			//type : maison,appartement
			$annonce[$i]['type'] = '';
			
			//titre
			$annonce[$i]['titre'] = trim($element->find('.summary a', 0)->plaintext);
			$annonce[$i]['prix'] = traitement_prix(trim($element->find('.price', 0)->plaintext));
			
			//url
			$annonce[$i]['url'] = trim($element->find('.summary a', 0)->href);
			
			//m2
			$annonce[$i]['m_carre'] = traitement_prix($element->find('.kiwii-clear-both', 1)->plaintext);
			
			//nb pièce
			$annonce[$i]['nb_piece'] = traitement_prix($element->find('.kiwii-clear-both', 0)->plaintext);
		
			$annonce[$i]['source'] = 'vivastreet';
			
			//type : appartement ou maison
			if(preg_match('#maison|villa|Propriété#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 2;
			}
			else if(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 1;
			}
			else if(preg_match('#immeuble#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 4;
			}
			else{
				$annonce[$i]['type'] = 0;
			}
			
			// rendement locatif
			if($annonce[$i]['type'] == 1){//appartement
				$annonce[$i]['rendement_locatif'] = round((($annonce[$i]['m_carre']*$prix_m2_locatif[0])*12)/$annonce[$i]['prix'],2)*100;
			}
			else{// maison et autre (immeuble, etc)
				$annonce[$i]['rendement_locatif'] = round((($annonce[$i]['m_carre']*$prix_m2_locatif[1])*12)/$annonce[$i]['prix'],2)*100;
			}
		
			$date = date("Y-m-d H:i:s");
			
		}
		$i++;
	
	}
	
	/* Affichage résultat */
	//Affichage des annonces
	//echo '<p>vivastreet url = '.$url.'</p>';
	/*echo '<pre>';
	print_r($annonce);
	echo '</pre>';*/
	
	return $annonce;
}


// scrapping seloger indicateur
function seloger_indicateur($_ville, $code_postal){
	
	if(preg_match('#[a-z0-9 ]+[0-9]{2}#',$_ville)){
		//$ville = substr($_ville, 0, -3);
		
		include 'arrondissement.php';
		$ville = $arrondissement[$code_postal]['seloger'];
		echo 'ville avec arrondissement';
	}
	else{
		echo 'ville sans arrondissement';
		$ville = $_ville;
		$ville = strtolower(str_replace('ST ','SAINT ',$ville));
		$ville = strtolower(str_replace(' ','-',$ville));
	}
	
	
	$departement = substr($code_postal, 0, 2);


	$url ='http://www.seloger.com/prix-immobilier/location/immo-'.$ville.'-'.$departement.'.htm';
	
	echo $url;
	
	$curl = curl_init(); 

	curl_setopt($curl, CURLOPT_URL, $url);  
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);  
	$str = curl_exec($curl);  
	curl_close($curl);  

	$html = str_get_html($str); 
	$info_m2 = array();
	foreach($html->find('.flatAndHouse .price') as $element){
		//echo $element;
		$tab = explode('€',$element->plaintext);
		//print_r($tab);
		$info_m2[] = trim($tab[0]);
		//echo trim($tab[0]);
	}
	
	// 0 valeur ville trop petite on met 0 sur les deux
	if(count($info_m2) == 0){
		$info_m2[0] = 0;
		$info_m2[1] = 0;
	}
	// si qu'une seule valeur => on met à 0 pour prix au m2 locatif maison
	else if(count($info_m2) == 1){
		$info_m2[1] = 0;
	}
	
	/* info_m2
	0 = appartement
	1 = maison
	*/

	return $info_m2;
	//echo '<p>Meilleur agent : '.$url.'</p>';
	echo $url;
	echo '<pre>';
	print_r($info_m2);
	echo '</pre>';
}
?>