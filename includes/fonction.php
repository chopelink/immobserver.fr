<?php
define('NOM_SITE', 'immObserver');
define('BASE_URL', 'https://dev.immobserver.fr');
define('HOUR_REFRESH', 5); // nombre d'heure depuis l'enregistrement de la dernière annonc pour re scrapper
define('FILE_SCRAPPING', $_SERVER['DOCUMENT_ROOT'].'/log/scrapping-log.txt'); // log sur les scrappings

ini_set('display_errors','on');
error_reporting(E_ALL);
date_default_timezone_set('Europe/Paris');
//https://data.opendatasoft.com/explore/dataset/code-postal-code-insee-2015%40public/api/


function recupere_ip() {
	if (getenv('HTTP_X_FORWARDED_FOR')) { return getenv('HTTP_X_FORWARDED_FOR'); }
	elseif (getenv('HTTP_CLIENT_IP')) { return getenv('HTTP_CLIENT_IP'); }
	else { return getenv('REMOTE_ADDR'); }
}


// traitement sur le prix scrapper pour supprimer tous les caractères inutiles
function traitement_prix($price){
	$prix = $price;
	$longueur = strlen($prix);
	$new_price = '';
	for ($i = 0; $i <= $longueur; $i++) {
		if(isset($prix[$i])){
			if(is_numeric($prix[$i])){
				$new_price .= $prix[$i];
			}
			else{}
		}
	}
	

	return $new_price;
}

function nettoyer_chiffre($chiffre){
	$chiffre = strip_tags($chiffre);
	$chiffre = trim($chiffre);
	if($chiffre == 'Nous contacter pour le prix'){
		$chiffre = 0;
	}else{
		$chiffre = str_replace(' ', '', $chiffre);
		$ExplodeEuro = explode('&euro;', $chiffre);
		$pos = strpos($ExplodeEuro[0], '€');
		if($pos !== false){
			$ExplodeEuroRec = explode('€', $ExplodeEuro[0]);
			$chiffre = $ExplodeEuroRec[0];
		}else{
			$chiffre = $ExplodeEuro[0];
		}
		
		$ListeValeurstest = array('0','1','2','3','4','5','6','7','8','9', '.', ',');
		$Arraychiffre = str_split($chiffre);
		$ArrayChiffreNettoyer = array();
		foreach($Arraychiffre as $ch){
			if(in_array($ch, $ListeValeurstest)){
				if($ch == ','){
					$ch = '.';
				}
				$ArrayChiffreNettoyer[] = $ch;
			}
		}
		
		$chiffre = implode('', $ArrayChiffreNettoyer);
	}
	return $chiffre;
}

function nettoyer_chiffre_superficie($chiffre){
	$chiffre = strip_tags($chiffre);
	$chiffre = trim($chiffre);
	$chiffre = str_replace(' ', '', $chiffre);
	$ExplodeM2 = explode('m2', $chiffre);
	$chiffre = $ExplodeM2[0];
	
	$ListeValeurstest = array('0','1','2','3','4','5','6','7','8','9', '.', ',');
	$Arraychiffre = str_split($chiffre);
	$ArrayChiffreNettoyer = array();
	foreach($Arraychiffre as $ch){
		if(in_array($ch, $ListeValeurstest)){
			$ArrayChiffreNettoyer[] = $ch;
		}
	}
	
	$chiffre = implode('', $ArrayChiffreNettoyer);
	return $chiffre;
}

function nettoyer_infos_safti($infos){
	$retour = array();
	$infos = strip_tags($infos);
	$infos = trim($infos);
	//$infos = str_replace(' ', '', $infos);
	
	//Séparation titre 
	$ExplodeTitre = explode('-', $infos);
	$Titre = '';
	$Titre = $ExplodeTitre[0];
	
	//Nbre piéces
	$pieces = 0;
	$ExplodePiece = explode(' ', $Titre);
	$DerniereElmt = $ExplodePiece[count($ExplodePiece)-4];
	$pieces = $DerniereElmt;
	
	//Superficie
	$Surface = 0;
	$Surface = nettoyer_chiffre_superficie($ExplodePiece[count($ExplodePiece)-2]);
	
	$retour['titre'] = $Titre;
	$retour['piece'] = $pieces;
	$retour['surface'] = $Surface;
	return $retour;
}

// traitement sur le prix scrapper pour supprimer tous les caractères inutiles et en gardant la virgule
function traitement_prix_virgule($price){
	$prix = $price;
	$longueur = strlen($prix);
	$new_price = '';
	for ($i = 0; $i <= $longueur; $i++) {
		if(isset($prix[$i])){
			if(is_numeric($prix[$i]) OR $prix[$i] == ','){
				$new_price .= $prix[$i];
			}
			else{}
		}
	}
	

	return $new_price;
}



function traitement_annonce($annonces){
	$liste_annonce = array();
	
	$i = 0;
	foreach($annonces as $element){
		if(is_numeric($element['prix']) AND $element['prix'] > 0 AND is_numeric($element['m_carre']) AND $element['m_carre'] > 0 ){
			$liste_annonce[$i]['type'] = $element['type'];
			$liste_annonce[$i]['titre'] = $element['titre'];
			$liste_annonce[$i]['prix'] = $element['prix'];
			$liste_annonce[$i]['url'] = $element['url'];
			$liste_annonce[$i]['m_carre'] = $element['m_carre'];
			$liste_annonce[$i]['nb_piece'] = $element['nb_piece'];
			$liste_annonce[$i]['rendement_locatif'] = $element['rendement_locatif'];
			$liste_annonce[$i]['source'] = $element['source'];
		}
		
		$i++;
	}
	
	return $liste_annonce;
}

/* affichage type de bien */
function type_bien($id){
	if($id == 0){
		return '<span class="label label-primary">Autres</span>';
	}
	
	//appartement
	else if($id == 1){
		return '<span class="label label-info">Appartement</span>';
	}
	
	//maison
	else if($id == 2){
		return '<span class="label label-warning">Maison</span>';
	}
	
	//maison
	else if($id == 11){
		return '<span class="label label-dark">Immeuble</span>';
	}
}


/* mise en forme<de la rentabilité locative */
function couleur_renta_locative($percent){
	//rentabilité locative supérieur à 7
	if($percent >= 7){
		return '<span class="label label-success">'.$percent.' %</span> ';
	}
	//rentabilité locative entre 4 et 8
	else if($percent <= 6 AND $percent >= 4){
		return '<span class="label label-default">'.$percent.' %</span>';
	}
	
	//rentabilité locative egal à 3
	else if($percent == 3){
		return '<span class="label label-warning">'.$percent.' %</span>';
	}
	
	//rentabilité locative inférieur à 3
	else if($percent <= 2 AND $percent >= 1){
		return '<span class="label label-danger">'.$percent.' %</span>';
	}
	// égal à 0 ou 0 car impossible de récupérer le m² locatif sur meilleur agent
	else if($percent == 0){
		return '<span class="label label-default">NC</span>';
	}
	else{
		return '<span class="label label-default">'.$percent.' %</span>';
	}
}

//verification abonnement
function info_abonnement($email){
	require 'sqlConnect.php';
	$req = $pdo->query('SELECT * FROM abonnement WHERE mail = "'.$email.'"');
	$data = $req->fetchObject();
	$req->closeCursor();
	return $data;
}


// nb annonces
function nb_annonce(){
	require 'sqlConnect.php';
	$requete = $pdo->query('SELECT COUNT(id_annonces_ventes) FROM annonces_ventes');
	$donnees = $requete->fetch();
	$nbAnnonce = $donnees[0];
	return $nbAnnonce;
	$requete->closeCursor();
}

// nb recherche
function nb_recherche(){
	require 'sqlConnect.php';
	$requete = $pdo->query('SELECT COUNT(id) FROM recherche');
	$donnees = $requete->fetch();
	$nbRecherche= $donnees[0];
	return $nbRecherche;
	$requete->closeCursor();
}

function ecrire_fichier_progression($fichier, $texte){
	if(!file_exists($fichier)){
		//Initialisation du fichier de progression 
		$File_progress = fopen($fichier, "a+");
		if($File_progress == false){
			die("La création du fichier de progression a échoué");
		}
		chmod($fichier, 0777);
	}
	file_put_contents($fichier, $texte);
}

function ecrire_fichier_log($fichier, $texte){
	$contents = lire_fichier_progression($fichier);
	$contents = $contents.$texte;
	file_put_contents($fichier, $contents);
}

function lire_fichier_progression($fichier){
	return file_get_contents($fichier);
}

function slugify($string, $replace = array(), $delimiter = '-') {
  // https://github.com/phalcon/incubator/blob/master/Library/Phalcon/Utils/Slug.php
  if (!extension_loaded('iconv')) {
    throw new Exception('iconv module not loaded');
  }
  // Save the old locale and set the new locale to UTF-8
  $oldLocale = setlocale(LC_ALL, '0');
  setlocale(LC_ALL, 'en_US.UTF-8');
  $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
  if (!empty($replace)) {
    $clean = str_replace((array) $replace, ' ', $clean);
  }
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
  $clean = strtolower($clean);
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
  $clean = trim($clean, $delimiter);
  // Revert back to the old locale
  setlocale(LC_ALL, $oldLocale);
  return $clean;
}

// recup des taux immobilier
function tx_immobilier(){
	require 'sqlConnect.php';
	$requete = $pdo->query('SELECT * FROM taux_immobilier ORDER BY date DESC LIMIT 1');
	$donnees = $requete->fetch(PDO::FETCH_ASSOC);
	$taux_immobilier = $donnees;
	return $taux_immobilier;
	$requete->closeCursor();
}

// contenu des formules
function contenu_abonnement($nom){
	$abonnement = array();
	
	if($nom == 'immo-essentiel'){
		$nb_recherche = 30;
		$nb_ville = 5;
		$nb_audit = 10;
		
	}
	else if($nom == 'immo-investisseur'){
		$nb_recherche = 85;
		$nb_ville = 10;
		$nb_audit = 25;
	}
	else if($nom == 'immo-rentier'){
		$nb_recherche = 200;
		$nb_ville = 25;
		$nb_audit = 50;
	}
	else if($nom == 'immo-platinium'){
		$nb_recherche = 500;
		$nb_ville = 100;
		$nb_audit = 50;
	}
	else if($nom == 'immo-gratuit'){
		$nb_recherche = 3;
		$nb_ville = 1;
		$nb_audit = 1;
	}
	
	$abonnement['nb_recherche'] = $nb_recherche;
	$abonnement['nb_ville'] = $nb_ville;
	$abonnement['nb_audit'] = $nb_audit;
	
	return $abonnement;
}

// calcul assurance
function calcul_assurance($taux_assurance,$prix,$duree){
	$montant_assurance['total'] = ($prix*(($taux_assurance/100)*$duree));
	$montant_assurance['mensuel'] = $montant_assurance['total']/($duree*12);
	return $montant_assurance;
}
?>