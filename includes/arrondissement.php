<?php
/*  Infos pour la cr�ation des urls en fonction des arrondissements pour le scrapping */

//paris
$arrondissement['75001'] = array('vivastreet' => 'paris-1er-ardt', 'meilleuragent' => 'paris-1e-arrondissement', 'seloger' => 'paris-1er');
$arrondissement['75002'] = array('vivastreet' => 'paris-2eme-ardt', 'meilleuragent' => 'paris-2e-arrondissement', 'seloger' => 'paris-2eme');
$arrondissement['75003'] = array('vivastreet' => 'paris-3eme-ardt', 'meilleuragent' => 'paris-3e-arrondissement', 'seloger' => 'paris-3eme');
$arrondissement['75004'] = array('vivastreet' => 'paris-4eme-ardt', 'meilleuragent' => 'paris-4e-arrondissement', 'seloger' => 'paris-4eme');
$arrondissement['75005'] = array('vivastreet' => 'paris-5eme-ardt', 'meilleuragent' => 'paris-5e-arrondissement', 'seloger' => 'paris-5eme');
$arrondissement['75006'] = array('vivastreet' => 'paris-6eme-ardt', 'meilleuragent' => 'paris-6e-arrondissement', 'seloger' => 'paris-6eme');
$arrondissement['75007'] = array('vivastreet' => 'paris-7eme-ardt', 'meilleuragent' => 'paris-7e-arrondissement', 'seloger' => 'paris-7eme');
$arrondissement['75008'] = array('vivastreet' => 'paris-8eme-ardt', 'meilleuragent' => 'paris-8e-arrondissement', 'seloger' => 'paris-8eme');
$arrondissement['75009'] = array('vivastreet' => 'paris-9eme-ardt', 'meilleuragent' => 'paris-9e-arrondissement', 'seloger' => 'paris-9eme');
$arrondissement['75010'] = array('vivastreet' => 'paris-10eme-ardt', 'meilleuragent' => 'paris-10e-arrondissement', 'seloger' => 'paris-10eme');
$arrondissement['75011'] = array('vivastreet' => 'paris-11eme-ardt', 'meilleuragent' => 'paris-11e-arrondissement', 'seloger' => 'paris-11eme');
$arrondissement['75012'] = array('vivastreet' => 'paris-12eme-ardt', 'meilleuragent' => 'paris-12e-arrondissement', 'seloger' => 'paris-12eme');
$arrondissement['75013'] = array('vivastreet' => 'paris-13eme-ardt', 'meilleuragent' => 'paris-13e-arrondissement', 'seloger' => 'paris-13eme');
$arrondissement['75014'] = array('vivastreet' => 'paris-14eme-ardt', 'meilleuragent' => 'paris-14e-arrondissement', 'seloger' => 'paris-14eme');
$arrondissement['75015'] = array('vivastreet' => 'paris-15eme-ardt', 'meilleuragent' => 'paris-15e-arrondissement', 'seloger' => 'paris-15eme');
$arrondissement['75016'] = array('vivastreet' => 'paris-16eme-ardt', 'meilleuragent' => 'paris-16e-arrondissement', 'seloger' => 'paris-16eme');
$arrondissement['75017'] = array('vivastreet' => 'paris-17eme-ardt', 'meilleuragent' => 'paris-17e-arrondissement', 'seloger' => 'paris-17eme');
$arrondissement['75018'] = array('vivastreet' => 'paris-18eme-ardt', 'meilleuragent' => 'paris-18e-arrondissement', 'seloger' => 'paris-18eme');
$arrondissement['75019'] = array('vivastreet' => 'paris-19eme-ardt', 'meilleuragent' => 'paris-19e-arrondissement', 'seloger' => 'paris-19eme');
$arrondissement['75020'] = array('vivastreet' => 'paris-20eme-ardt', 'meilleuragent' => 'paris-20e-arrondissement', 'seloger' => 'paris-20eme');

//lyon
$arrondissement['69001'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-1e-arrondissement', 'seloger' => 'lyon-1er');
$arrondissement['69002'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-2e-arrondissement', 'seloger' => 'lyon-2eme');
$arrondissement['69003'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-3e-arrondissement', 'seloger' => 'lyon-3eme');
$arrondissement['69004'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-4e-arrondissement', 'seloger' => 'lyon-4eme');
$arrondissement['69005'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-5e-arrondissement', 'seloger' => 'lyon-5eme');
$arrondissement['69006'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-6e-arrondissement', 'seloger' => 'lyon-6eme');
$arrondissement['69007'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-7e-arrondissement', 'seloger' => 'lyon-7eme');
$arrondissement['69008'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-8e-arrondissement', 'seloger' => 'lyon-8eme');
$arrondissement['69009'] = array('vivastreet' => 'lyon', 'meilleuragent' => 'lyon-9e-arrondissement', 'seloger' => 'lyon-9eme');

//marseille
$arrondissement['13001'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-1e-arrondissement', 'seloger' => 'marseille-1er');
$arrondissement['13002'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-2e-arrondissement', 'seloger' => 'marseille-2eme');
$arrondissement['13003'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-3e-arrondissement', 'seloger' => 'marseille-3eme');
$arrondissement['13004'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-4e-arrondissement', 'seloger' => 'marseille-4eme');
$arrondissement['13005'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-5e-arrondissement', 'seloger' => 'marseille-5eme');
$arrondissement['13006'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-6e-arrondissement', 'seloger' => 'marseille-6eme');
$arrondissement['13007'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-7e-arrondissement', 'seloger' => 'marseille-7eme');
$arrondissement['13008'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-8e-arrondissement', 'seloger' => 'marseille-8eme');
$arrondissement['13009'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-9e-arrondissement', 'seloger' => 'marseille-9eme');
$arrondissement['13010'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-10e-arrondissement', 'seloger' => 'marseille-10eme');
$arrondissement['13011'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-11e-arrondissement', 'seloger' => 'marseille-11eme');
$arrondissement['13012'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-12e-arrondissement', 'seloger' => 'marseille-12eme');
$arrondissement['13013'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-13e-arrondissement', 'seloger' => 'marseille-12eme');
$arrondissement['13014'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-14e-arrondissement', 'seloger' => 'marseille-14eme');
$arrondissement['13015'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-15e-arrondissement', 'seloger' => 'marseille-15eme');
$arrondissement['13016'] = array('vivastreet' => 'marseille', 'meilleuragent' => 'marseille-16e-arrondissement', 'seloger' => 'marseille-16eme');


?>