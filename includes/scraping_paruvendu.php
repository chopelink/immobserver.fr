<?php 

/**
 * https://www.paruvendu.fr/immobilier/annonceimmofo/liste/listeAnnonces?
 * tt=@variable_type
 * &tbApp=1
 * &tbDup=1
 * &tbChb=1
 * &tbLof=1
 * &tbAtl=1
 * &tbPla=1
 * &tbMai=1
 * &tbVil=1
 * &tbCha=1
 * &tbPro=1
 * &tbHot=1
 * &tbMou=1
 * &tbFer=1
 * &at=1
 * &pa=FR
 * &lol=0
 * &ray=50
 * &codeINSEE=@variable_code_insee
 *
 * ================================================
 * ///////////////// LOCATIONS ////////////////////
 * ================================================
 * tt=5
 *
 * ================================================
 * ///////////////// VENTES ///////////////////////
 * ================================================
 * tt=1
 *
 * ================================================
 * /////////////////// PROXY //////////////////////
 * ================================================
 * IP : 54.37.155.82
 * Port : 3128
 */

function scrapping_paruvendu_new(array $params){
	//Params PROXY 
	$Ip_proxy = '54.37.155.82';
	$Port_proxy = '3128';
	$conditions = '';
	
	//Code insee 'Obligatoire'
	if(isset($params['ci']) && !empty($params['ci'])){
		$ci = $params['ci'];
		$code_insee = $ci;
		$conditions .= '&codeINSEE='.$code_insee.'';
	}
	
	//Type d'annonce 
	$idtt = '';
	if(isset($params['idtt']) && !empty($params['idtt'])){
		$idtt = $params['idtt'];
		//Location
		if($idtt == 1){
			$conditions .= '&tt=5';
		}
		//Ventes
		elseif($idtt == 2){
			$conditions .= '&tt=1';
		}
	}
	
	//Pagination 
	$LISTING_LISTpg = 1;
	if(isset($params['pagination']) && !empty($params['pagination'])){
		$LISTING_LISTpg = $params['pagination'];
	}
	$conditions .= '&p='.$LISTING_LISTpg.'';
	
	$Url_paru_vendu = "https://www.paruvendu.fr/immobilier/annonceimmofo/liste/listeAnnonces?tbApp=1&tbDup=1&tbChb=1&tbLof=1&tbAtl=1&tbPla=1&tbMai=1&tbVil=1&tbCha=1&tbPro=1&tbHot=1&tbMou=1&tbFer=1&at=1&pa=FR&lol=0&ray=50".$conditions."";
	
	//Proxy
	$proxy = randomize_proxy();
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_USERAGENT, $proxy['agents']);
	curl_setopt($curl, CURLOPT_URL, $Url_paru_vendu);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_HTTPGET, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_PROXY, $proxy['proxy']);
	curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$str = curl_exec($curl);
	
	if(curl_errno($curl)){
		ecrire_fichier_log(FILE_SCRAPPING, "Paruvendu - ".date('Y-m-d H:i:s')." - Request Error:".curl_error($curl)." - ".$Url_paru_vendu."\n\r");
		return 'Request Error:' . curl_error($curl);
	}else{
		curl_close($curl);  
		$html = str_get_html($str);
		if(empty($html) || is_null($html)){
			ecrire_fichier_log(FILE_SCRAPPING, "Paruvendu - ".date('Y-m-d H:i:s')." - Résultats vide - ".$Url_paru_vendu." \n\r");
		}
		return $html;
	}
}

/**
 * Traitements des données HTML du site seloger 
 */
function traiter_data_paruvendu($html){
	global $compteurs;
	$compteurs++;
	$annonce = array();
	if(is_object($html)){
		//Liste des annonces
		$i = 0;
		foreach($html->find('.ergov3-annonce') as $element){
			$annonce[$i]['type'] = 0;
			$annonce[$i]['titre'] = '';
			$annonce[$i]['prix'] = 0;
			$annonce[$i]['url'] = '';
			$annonce[$i]['nb_chambres'] = 0;
			$annonce[$i]['m_carre'] = 0;
			$annonce[$i]['referrer'] = 'ParuVendu';
			$annonce[$i]['nb_piece'] = 0;
			
			$tab = explode(',',trim($element->find('h3', 0)->plaintext));
			
			if(!isset($tab[1])){//c'estune publicité et pas une annonce
			}
			else{
				$tab_m2 = explode(' ',$tab[1]);
				
				//titre
				$annonce[$i]['titre'] = $tab[0];
				
				//prix
				$annonce[$i]['prix'] = nettoyer_chiffre($element->find('.ergov3-priceannonce', 0)->plaintext);
				
				//url
				$annonce[$i]['url'] = 'https://www.paruvendu.fr'.$element->find('a', 1)->href;
				
				/*
				//maisons
				if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 2;
				}
				//Appartements
				elseif(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 1;
				}
				//Immeubles
				elseif(preg_match('#immeuble#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 11;
				}
				//Bâtiments
				elseif(preg_match('#bâtiment#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 12;
				}
				//Boutiques
				elseif(preg_match('#boutique#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 6;
				}
				//Local commercial
				elseif(preg_match('#commercial#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 7;
				}//Bureau
				elseif(preg_match('#bureau#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 8;
				}else{
					continue;
				}
				*/
				
				//maisons
				if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 2;
				}
				//Appartements
				elseif(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
					$annonce[$i]['type'] = 1;
				}
				else{
					continue;
				}
				
				$annonce[$i]['nb_piece'] = 0;
				$nbre_piece = traitement_prix($tab[0]);
				if(isset($nbre_piece) && !empty($nbre_piece)){
					$annonce[$i]['nb_piece'] = $nbre_piece;
				}
				
				$annonce[$i]['nb_chambres'] = 0;
				
				$annonce[$i]['m_carre'] = 0;
				$m_carre = nettoyer_chiffre_superficie($tab_m2[0]);
				if(isset($m_carre) && !empty($m_carre)){
					$annonce[$i]['m_carre'] = $m_carre;
				}

				//referrer (sources)
				$annonce[$i]['referrer'] = 'ParuVendu';
			}
			$i++;
		}
		return $annonce;
	}else{
		return array();
	}
}