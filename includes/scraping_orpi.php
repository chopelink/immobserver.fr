<?php
/**
 * ===========================================================================================================
 * SCRAPPING SITE : https://www.orpi.com
 * ===========================================================================================================
 * * Locations : https://www.orpi.com/recherche/rent?locations[]=valence&sort=date-down&layoutType=mixte
 * * * realEstateTypes[]=maison
 * * * &realEstateTypes[]=appartement
 * * * &realEstateTypes[]=terrain
 * * * &realEstateTypes[]=immeuble
 * * * &page=2
 *
 *
 * * Achats : https://www.orpi.com/recherche/buy?locations[]=valence&sort=date-down
 * * * &realEstateTypes[]=maison
 * * * &realEstateTypes[]=appartement
 * * * &realEstateTypes[]=terrain
 * * * &realEstateTypes[]=immeuble
 * * * &page=2
 * ===========================================================================================================
 * PROXY
 * ===========================================================================================================
 * IP : 54.37.155.82
 * Port : 3128
 */

function scrapping_orpi_new(array $params){
	//Params PROXY 
	$Ip_proxy = '54.37.155.82';
	$Port_proxy = '3128';
	$conditions = '';
	
	//Type d'annonce 
	$idtt = '';
	if(isset($params['idtt']) && !empty($params['idtt'])){
		$idtt = $params['idtt'];
		
		//Location
		if($idtt == 1){
			$conditions .= 'rent';
		}
		//Ventes ou Achat
		elseif($idtt == 2){
			$conditions .= 'buy';
		}
	}
	
	//Code insee 'Obligatoire' et nom de la ville
	if((isset($params['cp']) && !empty($params['cp'])) && (isset($params['ville']) && !empty($params['ville']))){
		$cp = $params['cp'];
		$code_postale = $cp;
		
		$ville = $params['ville'];
		$nom_ville_slugger = slugify($ville, array("'"));
		
		$conditions .= '?locations[]='.$nom_ville_slugger;
	}
	
	//Type de bien 
	$conditions .= '&realEstateTypes[]=maison&realEstateTypes[]=appartement&realEstateTypes[]=immeuble';
	
	//Pagination 
	$LISTING_LISTpg = 1;
	if(isset($params['pagination']) && !empty($params['pagination'])){
		$LISTING_LISTpg = $params['pagination'];
	}
	$conditions .= '&page='.$LISTING_LISTpg.'';
	
	$Url_orpi = 'https://www.orpi.com/recherche/'.$conditions.'&sort=date-down&layoutType=mixte';
	
	
	//Proxy
	$proxy = randomize_proxy();
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_USERAGENT, $proxy['agents']);
	curl_setopt($curl, CURLOPT_URL, $Url_orpi);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_HTTPGET, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_PROXY, $proxy['proxy']);
	curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$str = curl_exec($curl);
	
	if(curl_errno($curl)){
		ecrire_fichier_log(FILE_SCRAPPING, "Orpi - ".date('Y-m-d H:i:s')." - Request Error:".curl_error($curl)." - ".$Url_orpi."\n\r");
		return 'Request Error:' . curl_error($curl);
	}else{
		curl_close($curl);  
		$html = str_get_html($str);
		if(empty($html) || is_null($html)){
			ecrire_fichier_log(FILE_SCRAPPING, "Orpi - ".date('Y-m-d H:i:s')." - Résultats vide - ".$Url_orpi." \n\r");
		}
		return $html;
	}
}


/**
 * Traitements des données HTML du site orpi 
 */
function traiter_data_orpi($html){
	//file_put_contents($_SERVER['DOCUMENT_ROOT']."/tests/test.html",$html);
	global $compteurs;
	$compteurs++;
	$annonceList = '';
	if(gettype($html) == "object"){
		
		//Liste des annonces
		$i = 0;
		unlink($_SERVER['DOCUMENT_ROOT']."/tests/test.html");
		foreach($html->find('script') as $element){
			if($i == 5 || $i == 6){
				$txt = @file_get_contents($_SERVER['DOCUMENT_ROOT']."/tests/test.html");
				file_put_contents($_SERVER['DOCUMENT_ROOT']."/tests/test.html", $txt.''.str_replace(' ', '', $element));
			}
			
			$i++;
		}

		$annonceList = file_get_contents($_SERVER['DOCUMENT_ROOT']."/tests/test.html");
		preg_match_all('#items:\[(.*)\]#si', $annonceList, $results);
		$ObjAnnonces = $results[1][0];
		
		$ExplodeObjAnnonces = explode('},{', $ObjAnnonces);
		$ExplodeObjAnnoncesTraiter = array();
		$CaractereEnlever = array('{', '}', '"');
		foreach($ExplodeObjAnnonces as $TabAnnonces){
			$ExplodeObjAnnoncesTraiter[] = str_replace($CaractereEnlever, '', $TabAnnonces);
		}
		
		$ExplodeObjAnnoncesElement = array();
		foreach($ExplodeObjAnnoncesTraiter as $TabAnnoncesTraiter){
			$ExplodeTabAnnoncesTraiter = explode(',', $TabAnnoncesTraiter);
			$ExplodeObjAnnoncesElement[] = $ExplodeTabAnnoncesTraiter;
		}
		
		$ExplodeObjAnnoncesElementTraiter = array();
		$ListeObjAnnonces = array();
		$j = 0;
		foreach($ExplodeObjAnnoncesElement as $TabObjAnnoncesElement){
			foreach($TabObjAnnoncesElement as $TabAnnoncesElement){
				$ExplodeTabAnnoncesElement = explode(':', $TabAnnoncesElement);
				@$ListeObjAnnonces[$j][$ExplodeTabAnnoncesElement[0]] = $ExplodeTabAnnoncesElement[1];
			}
			$j++;
		}
		
		$annonce = array();
		$t = 0;
		foreach($ListeObjAnnonces as $Listes){
				
			//Type de bien et Titre
			if(isset($Listes['type']) && $Listes['type'] == 'maison'){
				$annonce[$t]['type'] = 2;
				if($Listes['nbRooms'] > 1){
					$annonce[$t]['titre'] = 'Maison '.$Listes['nbRooms'].' pièces';
				}else{
					$annonce[$t]['titre'] = 'Maison '.$Listes['nbRooms'].' pièce';
				}
			}elseif(isset($Listes['type']) && $Listes['type'] == 'appartement'){
				$annonce[$t]['type'] = 1;
				if($Listes['nbRooms'] > 1){
					$annonce[$t]['titre'] = 'Appartement '.$Listes['nbRooms'].' pièces';
				}else{
					$annonce[$t]['titre'] = 'Appartement '.$Listes['nbRooms'].' pièce';
				}
			}else{
				continue;
			}
			
			$annonce[$t]['prix'] = $Listes['price'];
			
			//Achats
			if($Listes['transactionType'] == 'buy'){
				$annonce[$t]['url'] = 'https://www.orpi.com/annonce-vente-'.$Listes['slug'];
			//Locations
			}elseif($Listes['transactionType'] == 'rent'){
				$annonce[$t]['url'] = 'https://www.orpi.com/annonce-location-'.$Listes['slug'];
			}else{
				$annonce[$t]['url'] = 'https://www.orpi.com/annonce-location-'.$Listes['slug'];
			}
			
			$annonce[$t]['nb_chambres'] = 0;
			$annonce[$t]['m_carre'] = $Listes['surface'];
			$annonce[$t]['referrer'] = 'Orpi';
			$annonce[$t]['nb_piece'] = $Listes['nbRooms'];
			
			$t++;
		}
		
		/*
		echo '<pre>';
		print_r($ListeObjAnnonces);
		echo '</pre>';
		*/
		
		
		return $annonce;
	}else{
		return array();
	}
}