<!-- Nav -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header"><a class="navbar-brand" href="<?= BASE_URL;?>"><img src="<?= BASE_URL;?>/images/logo.png" alt="Logo immobserver, logiciel immobilier locatif" /></a>
			<button type="button" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-left">
				<?php
				if(!isset($_SESSION['email'])){
					echo '<li><a href="#fonctionnalites">Fonctionnalités</a></li>';
				}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php
				if(isset($_SESSION['email'])){
				?>
				<li><a href="<?= BASE_URL;?>/search.php"><span class="glyphicon glyphicon-search"></span> Recherche</a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Bienvenue  <?= ucfirst($_SESSION['prenom']);?> <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="abonnement.php">Mon abonnement</a></li>
					<li><a href="save_search.php">Mes recherches</a></li>
					<li><a href="save_annonce.php">Annonces sauvegardées</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="deconnexion.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
				  </ul>
				</li>
				<?php
				}
				else{
					echo '
					<li><a href="login.php">Connexion</a></li>';
				}
				?>
				
			</ul>
			<?php
			if(!isset($_SESSION['email'])){
				echo '<p class="navbar-btn navbar-right"><a href="register.php" class="btn btn-nav"><b>Gratuit ! Inscrivez-vous</b></a></p>';
			}
			?>
		</div>
	</div>
</nav>
<!-- Nav -->