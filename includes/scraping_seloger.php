<?php
/**
 * =============================================
 * SCRAPPING SITE : http://www.seloger.com
 * =============================================
 * /////// FILTRE / Habitation /////////////////
 * - idtypebien : 	- Maison : 2
 *					- Appartement : 1
 * 				   	- Loft : 9
 *					- Hotel particulier : 14
 *					- Chateaux : 13
 *		   
 * NOTES : Filtres ensemble ou séparement
 * //////////////////////////////////////////////
 *
 * /////// FILTRE / Activité professionnelle ////
 * - idtypebien : 	- Boutique : 6
 * 					- Local commercial : 7
 * 					- Bureau : 8
 * NOTES : Filtres ensemble ou séparement
 * //////////////////////////////////////////////
 *
 * ////// FILTRE / Autres types de bien /////////
 * idtypebien : 	- Terrains : 4
 * 					- Parking / Box : 3
 * 					- Immeuble : 11
 * 					- Bâtiments : 12
 *   
 * NOTES : Filtres séparement
 * //////////////////////////////////////////////
 *
 * ////// FILTRE / Type d'achat /////////////////
 * - naturebien : 	- Ancien : 1
 * 					- Neuf : 2
 * 					- Construire : 4
 * 					- Luxe : 3
 * 					- Investissement : 1,2,4
 * 					- Viager : 1,2,4
 * //////////////////////////////////////////////
 *
 * ////// FILTRE / Type d'annonce ///////////////
 * idtt : 	- Louer : 1
 * 			- Acheter : 2
 * 			- Vendre : ??
 * //////////////////////////////////////////////
 *
 * /////// DIVERS FILTRES ///////////////////////
 * - pxmin : budget min
 * - pxmax : budget max
 * =====
 * - nb_pieces : Nbre piéce
 * - nb_chambres : Nbre chambres
 * =====
 * - surfacemin : surface min
 * - surfacemax : surface max
 * /////////////////////////////////////////////
 *
 * /////// FILTRE / Par code postale ///////////
 * - url : http://www.seloger.com/list.htm?org=advanced_search
 * - ci : Code postale Commune
 * - tri=initial
 * /////////////////////////////////////////////
 *
 *
 * =============================================
 * PROXY
 * =============================================
 * IP : 54.37.155.82
 * Port : 3128
 */

function scrapping_seloger_new(array $params){

	$conditions = '';
	
	//Code insee
	if(isset($params['ci']) && !empty($params['ci'])){
		$ci = $params['ci'];
		$code_insee = substr($ci,0,2).'0'.substr($ci,2,5);
		$conditions .= '&ci='.$code_insee.'';
	}else{
		$ci = '2238';
		$conditions .= '&div='.$ci.'';
	}
	
	//Type de bien
	$idtypebien = '';
	if(isset($params['idtypebien']) && !empty($params['idtypebien'])){
		$idtypebien = $params['idtypebien'];
		$conditions .= '&idtypebien='.$idtypebien.'';
	}
	
	//Type d'achat
	$naturebien = '';
	if(isset($params['naturebien']) && !empty($params['naturebien'])){
		$naturebien = $params['naturebien'];
		$conditions .= '&naturebien='.$naturebien.'';
	}
	
	//Type d'annonce 
	$idtt = '';
	if(isset($params['idtt']) && !empty($params['idtt'])){
		$idtt = $params['idtt'];
		$conditions .= '&idtt='.$idtt.'';
	}
	
	//Budget minimum
	$pxmin = '';
	if(isset($params['pxmin']) && !empty($params['pxmin'])){
		$pxmin = $params['pxmin'];
		$conditions .= '&pxmin='.$pxmin.'';
	}
	
	//Budget maximum
	$pxmax = '';
	if(isset($params['pxmax']) && !empty($params['pxmax'])){
		$pxmax = $params['pxmax'];
		$conditions .= '&pxmax='.$pxmax.'';
	}
	
	//Nombre des piéces
	$nb_pieces = '';
	if(isset($params['nb_pieces']) && !empty($params['nb_pieces'])){
		$nb_pieces = $params['nb_pieces'];
		$conditions .= '&nb_pieces='.$nb_pieces.'';
	}
	
	//Nombre des chambres
	$nb_chambres = '';
	if(isset($params['nb_chambres']) && !empty($params['nb_chambres'])){
		$nb_chambres = $params['nb_chambres'];
		$conditions .= '&nb_chambres='.$nb_chambres.'';
	}
	
	//Surface minimum
	$surfacemin = '';
	if(isset($params['surfacemin']) && !empty($params['surfacemin'])){
		$surfacemin = $params['surfacemin'];
		$conditions .= '&surfacemin='.$surfacemin.'';
	}
	
	//Surface maximum
	$surfacemax = '';
	if(isset($params['surfacemax']) && !empty($params['surfacemax'])){
		$surfacemax = $params['surfacemax'];
		$conditions .= '&surfacemax='.$surfacemax.'';
	}
	
	//Pagination 
	$LISTING_LISTpg = 1;
	if(isset($params['pagination']) && !empty($params['pagination'])){
		$LISTING_LISTpg = $params['pagination'];
	}
	$conditions .= '&LISTING-LISTpg='.$LISTING_LISTpg.'';
	
	$Url_seloger = "http://www.seloger.com/list.htm?org=advanced_search&tri=initial".$conditions."";
	
	//Proxy
	$proxy = randomize_proxy();

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_USERAGENT, $proxy['agents']);
	curl_setopt($curl, CURLOPT_URL, $Url_seloger);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_HTTPGET, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_PROXY, $proxy['proxy']);
	curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$str = curl_exec($curl);
	
	if(curl_errno($curl)){
		ecrire_fichier_log(FILE_SCRAPPING, "Seloger - ".date('Y-m-d H:i:s')." - Request Error:".curl_error($curl)." - ".$Url_seloger."\n\r");
		return 'Request Error:' . curl_error($curl);
	}else{
		curl_close($curl);  
		$html = str_get_html($str);
		if(empty($html) || is_null($html)){
			ecrire_fichier_log(FILE_SCRAPPING, "Seloger - ".date('Y-m-d H:i:s')." - Résultats vide - ".$Url_seloger."\n\r");
		}
		return $html;
	}
}

/**
 * Traitements des données HTML du site seloger 
 */
function traiter_data_seloger($html){
	global $compteurs;
	$compteurs++;
	$annonce = array();
	if(is_object($html)){
		//Liste des annonces
		$i = 0;
		foreach($html->find('.c-pa-list') as $element){
			$annonce[$i]['type'] = 0;
			$annonce[$i]['titre'] = '';
			$annonce[$i]['prix'] = 0;
			$annonce[$i]['url'] = '';
			
			
			//titre
			$annonce[$i]['titre'] = trim($element->find('.c-pa-link', 0)->plaintext);

			//prix
			$annonce[$i]['prix'] = nettoyer_chiffre($element->find('.c-pa-cprice',0)->plaintext);

			//url
			$annonce[$i]['url'] = $element->find('.c-pa-link', 0)->href;
			
			//maisons
			if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 2;
			}
			//Appartements
			elseif(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 1;
			}
			else{
				continue;
			}
			
			//Critéres
			$annonce[$i]['nb_piece'] = 0;
			$annonce[$i]['nb_chambres'] = 0;
			$annonce[$i]['m_carre'] = 0;
			foreach($element->find('.c-pa-criterion em') as $elmt){
				
				if(isset($elmt) && !empty($elmt)){
					
					$tab_elmt = explode(' ', strip_tags($elmt));
					$tab_elmt_value = trim($tab_elmt[0]);
					$tab_elmt_type = trim($tab_elmt[1]);
					
					//Nbre des piéces
					if($tab_elmt_type == 'p'){
						$annonce[$i]['nb_piece'] = $tab_elmt_value;
					}
					
					//Nbre des chambres
					if($tab_elmt_type == 'chb'){
						$annonce[$i]['nb_chambres'] = $tab_elmt_value;
					}
					
					//Surface
					if($tab_elmt_type == 'm²'){
						$annonce[$i]['m_carre'] = $tab_elmt_value;
					}
				}
			}
			
			//referrer (sources)
			$annonce[$i]['referrer'] = 'seloger';
			$i++;
		}
		return $annonce;
	}else{
		return array();
	}
}