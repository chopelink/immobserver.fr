<?php
/**
 * ===========================================================================================================
 * SCRAPPING SITE : https://www.safti.fr
 * ===========================================================================================================
 * * Locations : https://www.safti.fr/[idtt]/[liste type bien separer par -]/[localisation]/distance-0
 * * Achats : https://www.safti.fr/[idtt]/[liste type bien separer par -]/[localisation]/distance-0
 *
 * * Liste type bien : appartement-maison-immeuble
 * * Pagination : ?parameters=&page=2#listFilters
 * * idtt = acheter, louer
 * * localisation = slugify ville - cp ou slugify ville
 * ===========================================================================================================
 * PROXY
 * ===========================================================================================================
 * IP : 54.37.155.82
 * Port : 3128
 */

function scrapping_safti_new(array $params){
	global $arrondissement;
	
	//Params PROXY 
	$Ip_proxy = '54.37.155.82';
	$Port_proxy = '3128';
	$conditions = '';
	
	//Type d'annonce 
	$idtt = '';
	if(isset($params['idtt']) && !empty($params['idtt'])){
		$idtt = $params['idtt'];
		
		//Location
		if($idtt == 1){
			$conditions .= '/louer/';
		}
		//Ventes ou Achat
		elseif($idtt == 2){
			$conditions .= '/acheter/';
		}
	}
	
	//Type de bien 
	$conditions .= 'appartement-maison';
	
	//Code insee 'Obligatoire' et nom de la ville
	if((isset($params['cp']) && !empty($params['cp'])) && (isset($params['ville']) && !empty($params['ville']))){
		$Cp_grande_ville = array(75001,75002,75003,75004,75005,75006,75007,75008,75009,75010,75011,75012,75013,75014,75015,75016,75017,75018,75019,75020,69001,69002,69003,69004,69005,69006,69007,69008,69009,13001,13002,13003,13004,13005,13006,13007,13008,13009,13010,13011,13012,13013,13014,13015,13016);
		
		$cp = $params['cp'];
		$code_postale = $cp;
		
		$ville = $params['ville'];
		$nom_ville_slugger = slugify($ville, array("'"));
		
		//Les grandes villes
		if(in_array($code_postale, $Cp_grande_ville)){
			$conditions .= '/'.$arrondissement[$params['cp']]['meilleuragent'].'-'.$code_postale.'/distance-0';
		}else{
			$conditions .= '/'.$nom_ville_slugger.'-'.$code_postale.'/distance-0';
		}
	}
	
	//Pagination 
	$LISTING_LISTpg = 1;
	if(isset($params['pagination']) && !empty($params['pagination'])){
		$LISTING_LISTpg = $params['pagination'];
	}
	$conditions .= '/?parameters=&page='.$LISTING_LISTpg.'#listFilters';
	
	$Url_safti = 'https://www.safti.fr'.$conditions.'';
	
	//Proxy
	$proxy = randomize_proxy();
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_USERAGENT, $proxy['agents']);
	curl_setopt($curl, CURLOPT_URL, $Url_safti);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_HTTPGET, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_PROXY, $proxy['proxy']);
	curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$str = curl_exec($curl);
	
	if(curl_errno($curl)){
		ecrire_fichier_log(FILE_SCRAPPING, "Safti - ".date('Y-m-d H:i:s')." - Request Error:".curl_error($curl)." - ".$Url_safti."\n\r");
		return 'Request Error:' . curl_error($curl);
	}else{
		curl_close($curl);  
		$html = str_get_html($str);
		if(empty($html) || is_null($html)){
			ecrire_fichier_log(FILE_SCRAPPING, "Safti - ".date('Y-m-d H:i:s')." - Résultats vide - ".$Url_safti." \n\r");
		}
		return $html;
	}
}

/**
 * Traitements des données HTML du site safti 
 */
function traiter_data_safti($html){
	global $compteurs;
	$compteurs++;
	$annonce = array();
	if(is_object($html)){
		//Liste des annonces
		$i = 0;
		foreach($html->find('.property-list .property-thumbnail') as $element){
			$annonce[$i]['type'] = 0;
			$annonce[$i]['titre'] = '';
			$annonce[$i]['prix'] = 0;
			$annonce[$i]['url'] = '';
			$annonce[$i]['nb_chambres'] = 0;
			$annonce[$i]['m_carre'] = 0;
			$annonce[$i]['referrer'] = 'Safti';
			$annonce[$i]['nb_piece'] = 0;
			
			$Infos = nettoyer_infos_safti($element->find('.property-thumbnail--title a', 0)->plaintext);
			
			//titre
			$annonce[$i]['titre'] = $Infos['titre'];
			
			//prix
			$annonce[$i]['prix'] = nettoyer_chiffre($element->find('.property-thumbnail--price', 0)->plaintext);
			
			//url
			$annonce[$i]['url'] = $element->find('.property-thumbnail--title a', 0)->href;
			
			//maisons
			if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 2;
			}
			//Appartements
			elseif(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 1;
			}
			else{
				continue;
			}
			
			$annonce[$i]['nb_piece'] = 0;
			$nbre_piece = $Infos['piece'];
			if(isset($nbre_piece) && !empty($nbre_piece)){
				$annonce[$i]['nb_piece'] = (int)$nbre_piece;
			}
			
			$annonce[$i]['nb_chambres'] = 0;
			
			$annonce[$i]['m_carre'] = 0;
			$m_carre = $Infos['surface'];
			if(isset($m_carre) && !empty($m_carre)){
				$annonce[$i]['m_carre'] = $m_carre;
			}
			
			
			$i++;
		}
		return $annonce;
	}else{
		return array();
	}
}