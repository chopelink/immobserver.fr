<?php
/**
 * ===========================================================================================================
 * SCRAPPING SITE : https://fr.foncia.com
 * ===========================================================================================================
 * * Locations : https://fr.foncia.com/location/[nom ville slugger]-[code insee]/[type des biens]/[pagination]
 * * Achats : https://fr.foncia.com/achat/[nom ville slugger]-[code insee]/[type des biens]/[pagination]
 *
 * * Type de bien : appartement--maison--immeuble
 * * Pagination : page-1, page-1
 * ===========================================================================================================
 * PROXY
 * ===========================================================================================================
 * IP : 54.37.155.82
 * Port : 3128
 */

function scrapping_foncia_new(array $params){
	//Params PROXY 
	$Ip_proxy = '54.37.155.82';
	$Port_proxy = '3128';
	$conditions = '';
	
	//Type d'annonce 
	$idtt = '';
	if(isset($params['idtt']) && !empty($params['idtt'])){
		$idtt = $params['idtt'];
		
		//Location
		if($idtt == 1){
			$conditions .= '/location/';
		}
		//Ventes ou Achat
		elseif($idtt == 2){
			$conditions .= '/achat/';
		}
	}
	
	//Code insee 'Obligatoire' et nom de la ville
	if((isset($params['cp']) && !empty($params['cp'])) && (isset($params['ville']) && !empty($params['ville']))){
		$cp = $params['cp'];
		$code_postale = $cp;
		
		$ville = $params['ville'];
		$nom_ville_slugger = slugify($ville, array("'"));
		
		$conditions .= $nom_ville_slugger.'-'.$code_postale;
	}
	
	//Type de bien 
	$conditions .= '/appartement--maison/';
	
	//Pagination 
	$LISTING_LISTpg = 1;
	if(isset($params['pagination']) && !empty($params['pagination'])){
		$LISTING_LISTpg = $params['pagination'];
	}
	$conditions .= 'page-'.$LISTING_LISTpg.'';
	
	$Url_foncia = 'https://fr.foncia.com'.$conditions.'';
	
	//Proxy
	$proxy = randomize_proxy();
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_USERAGENT, $proxy['agents']);
	curl_setopt($curl, CURLOPT_URL, $Url_foncia);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_HTTPGET, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_PROXY, $proxy['proxy']);
	curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$str = curl_exec($curl);
	
	if(curl_errno($curl)){
		ecrire_fichier_log(FILE_SCRAPPING, "Foncia - ".date('Y-m-d H:i:s')." - Request Error:".curl_error($curl)." - ".$Url_foncia."\n\r");
		return 'Request Error:' . curl_error($curl);
	}else{
		curl_close($curl);  
		$html = str_get_html($str);
		if(empty($html) || is_null($html)){
			ecrire_fichier_log(FILE_SCRAPPING, "Foncia - ".date('Y-m-d H:i:s')." - Résultats vide - ".$Url_foncia." \n\r");
		}
		return $html;
	}
}

/**
 * Traitements des données HTML du site foncia 
 */
function traiter_data_foncia($html){
	global $compteurs;
	$compteurs++;
	$annonce = array();
	if(is_object($html)){
		//Liste des annonces
		$i = 0;
		foreach($html->find('.TeaserOffer') as $element){
			$annonce[$i]['type'] = 0;
			$annonce[$i]['titre'] = '';
			$annonce[$i]['prix'] = 0;
			$annonce[$i]['url'] = '';
			$annonce[$i]['nb_chambres'] = 0;
			$annonce[$i]['m_carre'] = 0;
			$annonce[$i]['referrer'] = 'Foncia';
			$annonce[$i]['nb_piece'] = 0;
			
			//titre
			$annonce[$i]['titre'] = $element->find('.TeaserOffer-content h3 a', 0)->plaintext;
			
			//prix
			$annonce[$i]['prix'] = nettoyer_chiffre($element->find('.TeaserOffer-price .TeaserOffer-price-num', 0)->plaintext);
			
			//url
			$annonce[$i]['url'] = 'https://fr.foncia.com'.$element->find('.TeaserOffer-content h3 a', 0)->href;
			
			//maisons
			if(preg_match('#maison|villa|Propriété|Ferme#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 2;
			}
			//Appartements
			elseif(preg_match('#appartement|t[1-8]|appart#i',$annonce[$i]['titre'])){
				$annonce[$i]['type'] = 1;
			}
			else{
				continue;
			}
			
			$annonce[$i]['nb_piece'] = 0;
			$nbre_piece = @$element->find('.TeaserOffer-content .MiniData .MiniData-row .MiniData-item', 1)->plaintext;
			if(isset($nbre_piece) && !empty($nbre_piece)){
				$annonce[$i]['nb_piece'] = (int)$nbre_piece;
			}
			
			$annonce[$i]['nb_chambres'] = 0;
			
			$annonce[$i]['m_carre'] = 0;
			$m_carre = @nettoyer_chiffre_superficie($element->find('.TeaserOffer-content .MiniData .MiniData-row .MiniData-item', 0)->plaintext);
			if(isset($m_carre) && !empty($m_carre)){
				$annonce[$i]['m_carre'] = $m_carre;
			}
			
			$i++;
		}
		return $annonce;
	}else{
		return array();
	}
}

/**
 * Scrapper avec les 5 chiffres du code postale
 * Si aucun reponse, on refait le scrapping avec les 2 premiers chiffres du code postale seulement
 */
function scrapping_foncia_recurssive($params){
	STATIC $count = 0;
	$count++;
	$EntitieHtml = scrapping_foncia_new($params);
	$donnees = traiter_data_foncia($EntitieHtml);
	if(count($donnees) == '0' && $count == '1'){
		$Arraycp = str_split($params['cp']);
		$params['cp'] = $Arraycp[0].$Arraycp[1];
		$donnees = scrapping_foncia_recurssive($params);
	}
	return $donnees;
}