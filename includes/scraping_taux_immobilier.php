<?php 

/**
 * https://www.empruntis.com/financement/actualites/barometres_regionaux.php
 */

function scrapping_taux_new(){
	$Url_taux = "https://www.empruntis.com/financement/actualites/barometres_regionaux.php";
	
	//Proxy
	$proxy = randomize_proxy();
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_USERAGENT, $proxy['agents']);
	curl_setopt($curl, CURLOPT_URL, $Url_taux);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_HTTPGET, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_PROXY, $proxy['proxy']);
	curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$str = curl_exec($curl);
	
	if(curl_errno($curl)){
		ecrire_fichier_log(FILE_SCRAPPING, "Taux immobilier - ".date('Y-m-d H:i:s')." - Request Error:".curl_error($curl)." - ".$Url_taux."\n\r");
		return 'Request Error:' . curl_error($curl);
	}else{
		curl_close($curl);  
		$html = str_get_html($str);
		if(empty($html) || is_null($html)){
			ecrire_fichier_log(FILE_SCRAPPING, "Taux immobilier - ".date('Y-m-d H:i:s')." - Résultats vide - ".$Url_taux." \n\r");
		}
		return $html;
	}
}

/**
 * Traitements des données HTML du site empruntis 
 */
function traiter_data_taux($html){
	global $compteurs;
	$compteurs++;
	$annonce = array();
	if(is_object($html)){
		//Liste des annonces
		$i = 0;
		foreach($html->find('table.tab_contenu tbody tr') as $element){
			$annonce[$i]['annee'] = 0;
			$annonce[$i]['taux_min'] = 0;
			$annonce[$i]['taux_marcher'] = 0;
			$annonce[$i]['taux_max'] = 0;
			
			$annonce[$i]['annee'] = (int)@$element->find('td[headers="th_tran"] div.titre_col_tab_contenu', 0)->plaintext;
			
			$annonce[$i]['taux_min'] = nettoyer_chiffre(@$element->find('td[headers="th_tauxmin"] div.tab_cell', 0)->plaintext);
			
			$annonce[$i]['taux_marcher'] = nettoyer_chiffre(@$element->find('td[headers="th_tauxmoy"] div.tab_cell', 0)->plaintext);
			
			$annonce[$i]['taux_max'] = nettoyer_chiffre(@$element->find('td[headers="th_tauxmax"] div.tab_cell', 0)->plaintext);
			
			$i++;
		}
		return $annonce;
	}else{
		return array();
	}
}