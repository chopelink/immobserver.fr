<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-language" content="fr" />
<meta name="language" content="fr" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!--<link href="https://bootswatch.com/yeti/bootstrap.css" rel="stylesheet">-->

<link href="<?= BASE_URL;?>/css/style2.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="icon" href="<?= BASE_URL;?>/images/favicon2.png">