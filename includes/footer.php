<footer class="footer">
  <div class="container">
  
    <div class="row">
		<div class="col-md-2">
		Immobserver est un outil de recherche de bien immobilier à rendement locatif élevé et d'aide à la décision d'investissemement basé sur de l'intelligence artificielle.
		</div>
		<div class="col-md-1">
		
		</div>
		
		<div class="col-md-3">
			<label>A PROPOS DE NOUS</label>
			<ul>
				<li><a href="<?= BASE_URL;?>/a-propos.html">A propos</a></li>
				<li><a href="<?= BASE_URL;?>/faq.html">Questions fréquentes</a></li>
				<li><a href="<?= BASE_URL;?>/mentions.html">Mentions légales</a></li>
				<li><a href="<?= BASE_URL;?>/cgu.html">CGU</a></li>
				<li><a href="<?= BASE_URL;?>">Nous contacter</a></li>
			</ul>
		</div>
		
		<div class="col-md-3">
			<label>DOCUMENTATION</label>
			<ul>
				<li><a href="<?= BASE_URL;?>/blog/">Blog</a></li>
				<li><a href="">API</a> <span class="label label-default">Bientôt</span></li>
			</ul>
		</div>
		
		<div class="col-md-3">
			<label>NOUS SUIVRE</label>
			<ul>
				<li><a href="https://twitter.com/immobserverFR" target="_blank">Twitter</a></li>
				<li><a href="">Linkedin</a></li>
				<li><a href="">Facebook</a></li>
			</ul>
		</div>
		
	</div><!-- row -->
	
	<div class="row">
		<div class="footer-line">
			<div class="col-md-9"><small>© 2016 - <?= date('Y');?> <?= NOM_SITE;?>. Tous droits réservés.</small>
				
			</div>
			<div class="col-md-3"></div>
		</div>
		
	</div>
  
	
</footer>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117247558-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117247558-1');
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
<!-- CAMMEMBERS GRAPHIQUES -->
<script src="js/charts/cammambers.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="js/main.js"></script>
<script src="js/md5/jquery.md5.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>