<?php
session_start();
if (!isset($_SESSION['email'])) {
	header ('Location: index.php');
	exit();
}
include 'includes/sqlConnect.php';
include 'includes/fonction.php';
require_once('stripe/config.php');

$abo = info_abonnement($_SESSION['email']);
$date_fin = new DateTime($abo->date_fin);
$date_fin = $date_fin->format('d/m/Y');

$message = '';
$resiliation = '';

if($abo->nb_recherche <= 0){
	$message = '<div class="alert alert-warning"><b>ATTENTION :</b> Vous avez <b>utilisé tous les crédits ce mois-ci</b> sur votre abonnement. Passez à une offre supérieur pour pouvoir continuer.</div>';
}

if(($abo->formule == 'immo-essentiel') OR ($abo->formule == 'immo-investisseur') OR ($abo->formule == 'immo-rentier') OR ($abo->formule == 'immo-platinium')){
	$resiliation = '<a href="/stripe/cancel.php" class="btn btn-default pull-right" onclick="return(confirm(\'Etes-vous sur de vouloir résilier votre abonnement ?\'));">Résilier mon abonnement</a>';
}

// information sur l'abonnement : nb_recherche, nb_audit, nb_ville, etc
$info_abo = contenu_abonnement($abo->formule);

/*echo '<pre>';
print_r($abo);
echo '</pre>';*/
?>
<html>
	<head>
		<title>Gestion de mon abonnement - <?= NOM_SITE;?></title>
		<?php include 'includes/meta.php';?>
		<script src="js/main.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
		<style>
* {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 25%;
    padding: 8px;
}

.price {
    list-style-type: none;
    border: 1px solid #eee;
    margin: 0;
    padding: 0;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.price:hover {
    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
    background-color: #444b54;
    color: white;
    font-size: 25px;
}

.price li {
    border-bottom: 1px solid #eee;
    padding: 20px;
    text-align: center;
}

.price .grey {
    background-color: #eee;
    font-size: 20px;
}

.button {
    background-color: #d35060;
    border: none;
    color: white;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}
</style>
	</head>
	<body>
	
		<?php include 'includes/navbar.php';?>
	
		<div class="container">

		
			<div class="row">
				<div class="col-md-12">
				<h1>Abonnement informations </h1>
				
				<?= $message;?>
				<?= $resiliation;?>
				<p>Abonnement actuel : <span class="label label-default"><?= ucfirst($abo->formule);?></span> <br />
				Nombre de recherche : <?= $abo->nb_recherche .'/'.$info_abo['nb_recherche'];?><br />
				Date de renouvellement : <?= $date_fin;?>
				</p>


		<div class="columns">
		<form action="stripe/charge.php" method="POST">
		  <ul class="price">
			<li class="header">Essentiel</li>
			<li class="grey">59€ / mois<br /><small style="font-size:14px;"><i>A peine le prix d'un plein d'essence ;-)</i></small></li>
			<li><b>5</b> villes</li>
			<li><b>50</b> recherches</li>
			<li>Sauvegarde des annonces</li>
			
			<li class="grey"><script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="<?php echo $stripe['publishable_key']; ?>"
    data-name="Abonnement Essentiel"
    data-description="Sans engagement de durée"
    data-amount="5900"
    data-label="Souscrire"
	data-locale="auto"
	data-image="https://immobserver.fr/images/favicon2.png"
	data-email="<?= $_SESSION['email'];?>"
	data-currency="eur">
  </script>
  <script>
   document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
   </script>
    <button type="submit" class="btn btn-nav">Souscrire</button>
  <input type="hidden" name="formule" value="1">
  </li>
		  </ul>
		  </form>
		</div>
	

	
		<div class="columns">
		  <form action="stripe/charge.php" method="POST">
		  <ul class="price">
			<li class="header" style="background-color:#46BAA7">Investisseur</li>
			<li class="grey"> 127€ / mois<br /><small style="font-size:14px;"><i>A peine le prix d'une bonne nuit d'hotel</i></small></li>
			<li><b>10</b> villes</li>
			<li><b>85</b> recherches</li>
			<li>Sauvegarde des annonces</li>
			<li class="grey">
			<script
			src="https://checkout.stripe.com/checkout.js" class="stripe-button"
			data-key="<?php echo $stripe['publishable_key']; ?>"
			data-name="Abonnement Investisseur"
			data-description="Sans engagement de durée"
			data-amount="12700"
			data-label="Souscrire"
			data-locale="auto"
			data-image="https://immobserver.fr/images/favicon2.png"
			data-email="<?= $_SESSION['email'];?>"
			data-currency="eur">
		  </script>
		  <script>
   document.getElementsByClassName("stripe-button-el")[1].style.display = 'none';
   </script>
    <button type="submit" class="btn btn-nav">Souscrire</button>
		   <input type="hidden" name="formule" value="2">
			</li>
		  </ul>
		  </form>
		</div>
	

	
		<div class="columns">
		  <form action="stripe/charge.php" method="POST">
		  <ul class="price">
			<li class="header">Rentier</li>
			<li class="grey"> 259€ / mois<br /><small style="font-size:14px;"><i>Le prix d'un billet de train</i></small></li>
			<li><b>30</b> villes</li>
			<li><b>200</b> recherches</li>
			<li>Sauvegarde des annonces</li>
			
			<li class="grey">
			<script
			src="https://checkout.stripe.com/checkout.js" class="stripe-button"
			data-key="<?php echo $stripe['publishable_key']; ?>"
			data-name="Abonnement Rentier"
			data-description="Sans engagement de durée"
			data-amount="25900"
			data-label="Souscrire"
			data-locale="auto"
			data-image="https://immobserver.fr/images/favicon2.png"
			data-email="<?= $_SESSION['email'];?>"
			data-currency="eur">
		  </script>
		  <script>
   document.getElementsByClassName("stripe-button-el")[2].style.display = 'none';
   </script>
    <button type="submit" class="btn btn-nav">Souscrire</button>
		   <input type="hidden" name="formule" value="3">
			</li>
		  </ul>
		  </form>
		</div>
		
		
	<div class="columns">
		  <form action="stripe/charge.php" method="POST">
		  <ul class="price">
			<li class="header">Platinium</li>
			<li class="grey"> 487€ / mois<br /><small style="font-size:14px;"><i>&nbsp;</i></small></li>
			<li>villes illimitées</li>
			<li><b>500</b> recherches</li>
			<li>Sauvegarde des annonces</li>
			
			<li class="grey">
			<script
			src="https://checkout.stripe.com/checkout.js" class="stripe-button"
			data-key="<?php echo $stripe['publishable_key']; ?>"
			data-name="Abonnement Platinium"
			data-description="Sans engagement de durée"
			data-amount="48700"
			data-label="Souscrire"
			data-locale="auto"
			data-image="https://immobserver.fr/images/favicon2.png"
			data-email="<?= $_SESSION['email'];?>"
			data-currency="eur">
		  </script>
		  <script>
   document.getElementsByClassName("stripe-button-el")[3].style.display = 'none';
   </script>
    <button type="submit" class="btn btn-nav">Souscrire</button>
		   <input type="hidden" name="formule" value="4">
			</li>
		  </ul>
		  </form>
		</div>
	
  

			<p class="text-center">Abonnement SANS ENGAGEMENT et sans durée limite</p>	
				</div><!--/ col-md-12 -->
				
				
			</div><!-- row -->
		
		</div><!-- container -->
		
		<?php include 'includes/footer.php';?>
	</body>
</html>